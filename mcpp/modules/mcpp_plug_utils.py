import os
import sys
import time
import configparser
import json
import __main__ as main


class PluginUtils:
    """Различные утилиты для плагинов."""
    def __init__(self, plugin):
        """

        :param plugin: название плагина
        """
        with main.grlock:
            self.PATH = os.path.join(main.PATH_PLUGINS, plugin)
            """Папка плагина в папке плагинов."""
        self.PATH_CONF = os.path.join(self.PATH, 'config.ini')
        """Путь к конфигу."""
        self.config = configparser.ConfigParser()
        """ConfigParser (ini) конфиг плагина."""

    @staticmethod
    def time_task(strtime, task, event, args=(), once=False):
        """Выполнение кода в заданное время суток, для использования в потоке
        или процессе. Статический метод.

        :param strtime: время, в которое происходит выполнение (H:M:S)
        :param task: объект
        :param event: Event, при установке которого цикл завершается
        :param args: аргументы, передаваемые при создании объекта
        :param once: выполнить ли один раз (False)
        """
        while True:
            if event.is_set(): break
            if time.strftime('%H:%M:%S') == strtime:
                task(*args)
                if once: break
            time.sleep(1)

    @staticmethod
    def multi_time_task(tasks, event):
        """Выполнение кода в заданное время суток, для использования в потоке
        или процессе. Статический метод.

        :param tasks: словарь с данными:
        {время: [[объект, (аргумент, ...), once], ...]}.
        once - bool, при True код выполняется один раз. Формат времени: H:M:S
        Пример значения: {'01:15:00': [[func, ('one', 2, 3), False]
        :param event: Event, при установке которого цикл завершается
        """
        while True:
            if event.is_set(): break
            if not tasks: break
            t = time.strftime('%H:%M:%S')
            if t in tasks:
                for task in tasks[t]:
                    task[0](*task[1])
                    if task[2]: tasks[t].remove(task)
                if not tasks[t]: del tasks[t]
            time.sleep(1)

    @staticmethod
    def timer_task(startsleep, interval, task, event, args=(), once=False):
        """Выполнение кода через заданный интервал, для использования в потоке
        или процессе. Статический метод.

        :param startsleep: hort (секунды), время задержки перед запуском цикла
        :param interval: short (секунды), интервал выполнения кода
        :param task: объект
        :param event: Event, при установке которого цикл завершается
        :param args: аргументы, передаваемые при создании объекта
        :param once: выполнить ли один раз (False)
        """
        time.sleep(startsleep)
        while True:
            if event.is_set(): break
            task(*args)
            if once: break
            time.sleep(interval)

    def make_folder(self):
        """Создать папку плагина."""
        try:
            if not os.path.isdir(self.PATH): os.makedirs(self.PATH)
        except BaseException as be:
            with main.grlock:
                main.debug.debug(json.dumps(str(be.args)))

    def is_config(self):
        """Существует ли конфиг."""
        try:
            return os.path.isfile(self.PATH_CONF)
        except BaseException as be:
            with main.grlock:
                main.debug.debug(json.dumps(str(be.args)))

    def read_config(self):
        """Прочитать конфиг."""
        try:
            self.make_folder()
            if self.is_config(): self.config.read(self.PATH_CONF)
        except BaseException as be:
            with main.grlock:
                main.debug.debug(json.dumps(str(be.args)))

    def save_config(self):
        """Сохранить конфиг."""
        try:
            self.make_folder()
            with open(self.PATH_CONF, 'w') as c: self.config.write(c)
        except BaseException as be:
            with main.grlock:
                main.debug.debug(json.dumps(str(be.args)))


class PluginAPI:
    """Класс с элементами доступа к компонентам ПУ."""
    def __init__(self, pm):
        """

        :param pm: PluginManager
        """
        self.pmanager = pm
        """Объект PluginManager - класс, управляющий плагинами."""
        self.mcppworker = None
        """Объект MCPPWorker - класс, управляющий ПУ."""
        self.mcppserver = None
        """Объект MCPPServer - класс, управляющий сетевыми соединениями."""
        self.mcppstarter = None
        """Объект MCPPStarter - класс, запускающий и останавливающий ПУ."""
        self.module = sys.modules['__main__'].__dict__
        """Словарь с элементами модуля ПУ, для полного доступа ко всем
        компонентам. Ключами служат названия."""
