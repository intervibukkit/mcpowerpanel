from enum import IntEnum


class ReqResult(IntEnum):
    """Результат выполнения запроса или сообщение для отправки клиенту."""
    broken = 0
    notfound = 1
    disabled = 2
    wrongpass = 3
    error = 4
    good = 5
    noperm = 6
    none = 7
    noreply = 8
    banned = 9
    full = 10
    timeout = 11
    notplugin = 12
    badhost = 13
    close = 14
    logged = 15
    ping = 16
    pong = 17


class EventAction(IntEnum):
    """Тип возвращаемого значения при вызове событий из плагинов (action).

    Если нужно заменить данные, то передается произвольное значение."""
    none = 0
    allow = 1
    deny = 2


class EventsPriority(IntEnum):
    """Приоритет отпрвки событий плагину."""
    high = 0
    medium = 1
    low = 2


class BanReason(IntEnum):
    """Причина бана IP."""
    max_try = 0
    max_err = 1
    max_login_err = 2


class KickReason(IntEnum):
    """Причина кика пользователя."""
    timeout = 0
    timeout_auth = 1
    root = 2


class StopReason(IntEnum):
    """Причина остановки панели."""
    command = 0
    crash = 1
    other = 2


class ElementType(IntEnum):
    """Типы элементов страницы."""
    label = 0
    text = 1
    textbox = 2
    textarea = 3
    button = 4
    checkbox = 5
