import __main__ as main
from mcpp.modules.sub_worker.worker_base import *
from mcpp.modules.mcpp_utils import *


class MCPPWorker(MCPPWorkerBase):
    """Обработчик клиент-серверных запросов."""
    def login(self, data, addr):
        """Авторизация пользователя. Если она прошла успешно, пользователь
        добавляется в users.

        :param data: данные в виде json списка со строками,
        см. док. client-server
        :param addr: (IP, порт)
        :return: значение ReqResult
        """
        try:
            if not data or not addr:
                return ReqResult.broken, None
            elif type(data) != str and type(addr) != tuple:
                return ReqResult.broken, None
            with main.grlock:
                main.debug.debug(json.dumps([data, addr]))
            u = json.loads(data)
            # проверки
            if len(u) != 2:
                return ReqResult.broken, None
            if u[0] == 'ROOT':  # супер-админ
                if self.check_root():
                    with main.grlock:
                        host = main.config['ROOT']['host']
                        if host == 'localhost': host = '127.0.0.1'
                        if host != '0.0.0.0' and addr[0] != host:
                            main.debug.warning(
                                json.dumps(['badhost', data, addr]))
                            return ReqResult.badhost, u[0]
                        a = AESCip(hashpass=main.config['ROOT']['pass'],
                                   encoding=main.config['MAIN']['encoding'])
                else:
                    self.wlog.error(json.dumps([u, ReqResult.disabled.name]))
                    return ReqResult.disabled, u[0]
            else:  # пользователь
                with main.grlock:
                    if u[0] not in main.users:
                        self.wlog.error(
                            json.dumps([u, ReqResult.notfound.name]))
                        return ReqResult.notfound, u[0]
                    if main.users[u[0]]['group'] not in main.groups:
                        self.wlog.error(json.dumps([u, 'notfound group']))
                        return ReqResult.notfound, u[0]
                    if main.users[u[0]]['enable'] != 'yes':
                        ge = main.groups[main.users[u[0]]['group']]['enable']
                        if ge != 'yes':
                            self.wlog.error(json.dumps([u, 'disabled group']))
                            return ReqResult.disabled, u[0]
                        self.wlog.error(
                            json.dumps([u, ReqResult.disabled.name]))
                        return ReqResult.disabled, u[0]
                    a = AESCip(hashpass=main.users[u[0]]['pass'],
                               encoding=main.config['MAIN']['encoding'])
            # проверка пароля
            if a.dec(u[1]) == "Hello, MCPP! I'm " + u[0] + '.':
                # если дешифровка удалась
                if u[0] in self.users:  # если пользователь уже залогинен
                    with main.grlock:
                        main.debug.warning(json.dumps(['deny', u, addr]))
                    self._auth(u[0], addr, EventAction.deny)
                    return ReqResult.logged, u[0]
                action = self._auth(u[0], addr, EventAction.allow)
                if action == EventAction.deny:
                    with main.grlock:
                        main.debug.warning(json.dumps(['deny', u, addr]))
                    return ReqResult.none, u[0]
                # успешная авторизация
                with main.grlock:
                    main.debug.warning(json.dumps(['ok', u, addr]))
                self.users[u[0]] = a
                self.names[u[0]] = addr
                self.wlog.critical(json.dumps([u, ReqResult.good.name]))
                return ReqResult.good, u[0]
            else:  # дешифровка не удалась / передано не то приветствие
                with main.grlock:
                    main.debug.warning(json.dumps(['fail', u, addr]))
                self._auth(u[0], addr, EventAction.deny)
                self.wlog.critical(json.dumps([u, ReqResult.wrongpass.name]))
                return ReqResult.wrongpass, u[0]
        except BaseException as be:
            # что-то пошло не так, например, данные не json формата
            with main.grlock:
                main.debug.error(json.dumps(['except', addr, str(be.args)]))
            return ReqResult.error, None

    def _com(self, d, user):
        """Обработка запросов к ПУ.

        :param d: данные в виде списка строк
        :param user: имя пользователя
        :return: различные данные, см. док. client-server
        """
        with main.grlock:
            main.debug.debug(json.dumps([d, user]))

        # обработка запросов
        if d[1] == 'list':  # запрос списка серверов
            if self.check_perm(user, 'stats'):
                if self.get_perm(user, 'main.servers') == '-':
                    return ReqResult.noperm
                result = []
                with main.grlock:
                    sl = main.servers.keys()
                for s in sl:
                    if s == 'DEFAULT': continue
                    if not self.check_serv(user, s): continue
                    result.append(s)
                if not result: return ReqResult.notfound
                return result
            else: return ReqResult.noperm
        elif d[1] == 'list_run':  # запущенных серверов
            return self.list_(user, True)
        elif d[1] == 'list_stop':  # остановленных серверов
            return self.list_(user, False)
        elif d[1] == 'status':  # запрос состояния сервера
            if len(d) != 3: return ReqResult.broken
            if self.check_perm(user, 'stats'):
                if d[2] not in self.sm.servs: return ReqResult.notfound
                if not self.check_serv(user, d[2]): return ReqResult.noperm
                serv = self.sm.servs[d[2]]
                try:
                    serv.rlock.acquire()
                    stop = serv.stop_event.is_set()
                    if serv.proc:
                        proc = serv.proc.is_alive()
                    else: proc = False
                    subproc = serv.subproc.is_alive()
                finally: serv.rlock.release()
                return [stop, proc, subproc]
            else: return ReqResult.noperm
        elif d[1] == 'date_start_all':  # запрос даты старта серверов
            return self.date_(user, True)
        elif d[1] == 'date_stop_all':  # запрос даты остановки серверов
            return self.date_(user, False)
        elif d[1] == 'date_start':  # запрос даты старта сервера
            return self.date_serv(user, True, d)
        elif d[1] == 'date_stop':  # запрос даты остановки
            return self.date_serv(user, False, d)
        elif d[1] == 'start_all':  # запустить все сервера
            return self.start_stop(user, True)
        elif d[1] == 'start':  # запустить сервер
            if len(d) != 3: return ReqResult.broken
            return self.start_stop(user, True, d[2])
        elif d[1] == 'stop_all':  # остановить все сервера
            return self.start_stop(user, False)
        elif d[1] == 'stop':  # остановить сервер
            if len(d) != 3: return ReqResult.broken
            return self.start_stop(user, False, d[2])
        elif d[1] == 'kill_all':  # убить процессы всех серверов
            return self.kill(user)
        elif d[1] == 'kill':  # убить процесс сервера
            if len(d) != 3: return ReqResult.broken
            return self.kill(user, d[2])
        elif d[1] == 'out':  # запрос вывода сервера
            if len(d) != 3: return ReqResult.broken
            if self.check_perm(user, 'cons_see'):
                if d[2] not in self.sm.servs: return ReqResult.notfound
                if not self.check_serv(user, d[2]): return ReqResult.noperm
                serv = self.sm.servs[d[2]]
                try:
                    serv.rlock.acquire()
                    out = []
                    for o in serv.out: out.append(o)
                finally: serv.rlock.release()
                return out
            else: return ReqResult.noperm
        elif d[1] == 'chat':  # послать сообщение в чат
            if len(d) != 4: return ReqResult.broken
            if self.check_perm(user, 'chat_speak'):
                if d[2] not in self.sm.servs: return ReqResult.notfound
                if not self.check_serv(user, d[2]): return ReqResult.noperm
                self.sm.servs[d[2]].chat(d[3])
                return ReqResult.good
            else: return ReqResult.noperm
        elif d[1] == 'chat_all':  # послать сообщение в чат всех серверов
            if len(d) != 3: return ReqResult.broken
            if self.check_perm(user, 'chat_speak'):
                with main.grlock: sl = main.servers.keys()
                result = []
                for serv in sl:
                    if serv == 'DEFAULT': continue
                    if serv not in self.sm.servs: continue
                    if not self.check_serv(user, serv): continue
                    self.sm.servs[serv].chat(d[2])
                    result.append(serv)
                if not result: return ReqResult.notfound
                return result
            else: return ReqResult.noperm
        elif d[1] == 'input':  # послать команду на ввод
            if len(d) != 4: return ReqResult.broken
            com = self.get_perm(user, 'com_list').split('|')
            if com[0] == '-': return ReqResult.noperm
            elif com[0] == '.' or d[3] in com or com[0] == 'ROOT':
                if d[2] not in self.sm.servs: return ReqResult.notfound
                if not self.check_serv(user, d[2]): return ReqResult.noperm
                self.sm.servs[d[2]].put(d[3])
                return ReqResult.good
            else: return ReqResult.noperm
        elif d[1] == 'input_all':  # послать команду на все сервера
            if len(d) != 3: return ReqResult.broken
            com = self.get_perm(user, 'com_list').split('|')
            if com[0] == '-': return ReqResult.noperm
            elif com[0] == '.' or d[2] in com or com[0] == 'ROOT':
                with main.grlock: sl = main.servers.keys()
                result = []
                for serv in sl:
                    if serv == 'DEFAULT': continue
                    if serv not in self.sm.servs: continue
                    if not self.check_serv(user, serv): continue
                    self.sm.servs[serv].put(d[2])
                    result.append(serv)
                if not result: return ReqResult.notfound
                return result
            else: return ReqResult.noperm
        elif d[1] == 'restart_all':  # перезапустить все сервера
            return self.restart(user)
        elif d[1] == 'restart':  # перезапустить сервер
            if len(d) != 3: return ReqResult.broken
            return self.restart(user, d[2])
        elif d[1] == 'lim':  # запрос лимита на хранение вывода
            if len(d) != 3: return ReqResult.broken
            if self.check_perm(user, 'stats'):
                if d[2] not in self.sm.servs: return ReqResult.notfound
                if not self.check_serv(user, d[2]): return ReqResult.noperm
                return self.sm.servs[d[2]].get_lim()
            else: return ReqResult.noperm
        elif d[1] == 'setlim':  # установить лимит хранения вывода
            if len(d) != 4: return ReqResult.broken
            if self.check_perm(user, 'control'):
                if d[2] not in self.sm.servs: return ReqResult.notfound
                if not self.check_serv(user, d[2]): return ReqResult.noperm
                self.sm.servs[d[2]].set_lim(d[3])
                return ReqResult.good
            else: return ReqResult.noperm
        elif d[1] == 'serv_pids':  # запрос PID-ов процессов всех серверов
            return self.get_pid(user)
        elif d[1] == 'serv_pid':  # запрос PID-а сервера
            if len(d) != 3: return ReqResult.broken
            return self.get_pid(user, d[2])
        elif d[1] == 'proc_pids':  # запрос PID-ов всех обслуживающих процессов
            return self.get_pid(user, sub=False)
        elif d[1] == 'proc_pid':  # запрос PID-a обслуживающего процесса
            if len(d) != 3: return ReqResult.broken
            return self.get_pid(user, d[2], False)
        elif d[1] == 'main.users':  # запрос списка пользователей
            if user == 'ROOT' and self.check_root():
                return self.users.keys()
            else: return ReqResult.noperm
        elif d[1] == 'kick':  # кикнуть пользователя
            if user == 'ROOT' and self.check_root():
                if len(d) != 3: return ReqResult.broken
                self._kick(self.names[d[2]], KickReason.root)
                try:
                    self._conn[self.names[d[2]]].shutdown(socket.SHUT_RDWR)
                    return ReqResult.good
                except OSError: return ReqResult.error
            else: return ReqResult.noperm
        elif d[1] == 'disable_user':  # отключить пользователя
            return self.enable_user(user, d)
        elif d[1] == 'enable_user':  # включить пользователя
            return self.enable_user(user, d, True)
        elif d[1] == 'newpass':  # сменить пароль пользователю
            if len(d) != 4: return ReqResult.broken
            if user == 'ROOT' and self.check_root():
                 # ROOT только через newpass_user
                if d[2] == 'ROOT': return ReqResult.none
                with main.grlock:
                    if d[2] not in main.users: return ReqResult.notfound
                     # передается не пароль, а hex хэш
                    main.users[d[2]]['pass'] = d[3]
                    with open(main.PATH_USERS, 'w') as users_:
                        main.users.write(users_)
                    return ReqResult.good
            else: return ReqResult.noperm
        elif d[1] == 'newpass_user':  # сменить свой пароль
            if len(d) != 4: return ReqResult.broken
            with main.grlock:
                if user == 'ROOT':
                    main.config['ROOT']['pass'] = d[3]
                    with open(main.PATH_CONF, 'w') as conf:
                        main.config.write(conf)
                else:
                    if d[2] != main.users[user]['pass']:
                        return ReqResult.wrongpass
                    main.users[user]['pass'] = d[3]
                    with open(main.PATH_USERS, 'w') as users_:
                        main.users.write(users_)
                return ReqResult.good
        elif d[1] == 'reg':  # зарегистрировать пользователя
            if len(d) != 6: return ReqResult.broken
            if user == 'ROOT' and self.check_root():
                with main.grlock:
                    main.users[d[2]] = {
                        'group': d[3],
                        'enable': d[4],
                        'pass': d[5]
                        }
                    with open(main.PATH_USERS, 'w') as users_:
                        main.users.write(users_)
                    return ReqResult.good
            else: return ReqResult.noperm
        elif d[1] == 'setgroup':  # установить группу пользователю
            if len(d) != 4: return ReqResult.broken
            if user == 'ROOT' and self.check_root():
                with main.grlock:
                    if d[2] not in main.users: return ReqResult.notfound
                    if d[3] not in main.groups: return ReqResult.notfound
                    main.users[d[2]]['group'] = d[3]
                    with open(main.PATH_USERS, 'w') as users_:
                        main.users.write(users_)
                    return ReqResult.good
            else: return ReqResult.noperm
        elif d[1] == 'del':  # удалить пользователя
            if len(d) != 3: return ReqResult.broken
            if user == 'ROOT' and self.check_root():
                with main.grlock:
                    if d[2] not in main.users: return ReqResult.notfound
                    del main.users[d[2]]
                    with open(main.PATH_USERS, 'w') as users_:
                        main.users.write(users_)
                    return ReqResult.good
            else: return ReqResult.noperm
        elif d[1] == 'addperm':  # добавить разрешение группе
            if len(d) != 5: return ReqResult.broken
            if user == 'ROOT' and self.check_root():
                with main.grlock:
                    main.groups[d[2]] = {d[3]: d[4]}
                    with open(main.PATH_GROUPS, 'w') as g: main.groups.write(g)
                    return ReqResult.good
            else: return ReqResult.noperm
        elif d[1] == 'delperm':  # удалить разрешение у группы
            if len(d) != 4: return ReqResult.broken
            if user == 'ROOT' and self.check_root():
                with main.grlock:
                    if d[2] not in main.groups: return ReqResult.notfound
                    if d[3] not in main.groups[d[2]]: return ReqResult.notfound
                    del main.groups[d[2]][d[3]]
                    with open(main.PATH_GROUPS, 'w') as g: main.groups.write(g)
                    return ReqResult.good
            else: return ReqResult.noperm
        elif d[1] == 'enable_perm':  # включить разрешение
            return self.enable_perm(user, d)
        elif d[1] == 'disable_perm':  # выключить разрешение
            return self.enable_perm(user, d, False)
        elif d[1] == 'addgroup':  # добавить группу пользователей
            if len(d) != 11: return ReqResult.broken
            if user == 'ROOT' and self.check_root():
                with main.grlock:
                    main.groups[d[2]] = {
                        'control': d[3],
                        'servers': d[4],
                        'stats': d[5],
                        'chat_speak': d[6],
                        'plugins': d[7],
                        'com_list': d[8],
                        'enable': d[9],
                        'cons_see': d[10]
                        }
                    with open(main.PATH_GROUPS, 'w') as g: main.groups.write(g)
                    return ReqResult.good
            else: return ReqResult.noperm
        elif d[1] == 'remgroup':  # удалить группу
            if len(d) != 3: return ReqResult.broken
            if user == 'ROOT' and self.check_root():
                with main.grlock:
                    if d[2] not in main.groups: return ReqResult.notfound
                    del main.groups[d[2]]
                    with open(main.PATH_GROUPS, 'w') as g: main.groups.write(g)
                    return ReqResult.good
            else: return ReqResult.noperm
        elif d[1] == 'enable_group':  # включить группу
            return self.enable_group(user, d)
        elif d[1] == 'disable_group':  # выключить группу
            return self.enable_group(user, d, False)
        elif d[1] == 'reload_configs':  # перезагрузить конфиги
            if user == 'ROOT' and self.check_root():
                with main.grlock:
                    main._config()
                return ReqResult.good
            else: return ReqResult.noperm
        elif d[1] == 'get_plugins':  # получить список плагинов
            result = []
            for p in self._plugins:
                if self.check_plugin(user, p): result.append(p)
            if not result: return ReqResult.notfound
            return result
        elif d[1] == 'get_func':  # получить функции плагина
            if len(d) != 3: return ReqResult.broken
            if d[2] not in self._plugins: return ReqResult.notfound
            if self.check_plugin(user, d[2]): return self._func(d[2])
            else: return ReqResult.noperm
        elif d[1] == 'get_info':  # получить данные о плагине
            if len(d) != 3: return ReqResult.broken
            if d[2] not in self._plugins: return ReqResult.notfound
            if self.check_plugin(user, d[2]):
                plug = self._plugins[d[2]]
                info = [
                    plug.AUTHOR,
                    plug.EMAIL,
                    plug.LICENSE,
                    plug.VERSION,
                    plug.WEBSITE,
                    plug.DESCRIPTION
                    ]
                return info
            else: return ReqResult.noperm
        elif d[1] == 'load_all':  # загрузить все плагины
            if user == 'ROOT' and self.check_root():
                if self.load(): return ReqResult.good
                else: return ReqResult.none
            else: return ReqResult.noperm
        elif d[1] == 'unload_all':  # отгрузить все плагины
            if user == 'ROOT' and self.check_root():
                if self.unload(): return ReqResult.good
                else: return ReqResult.none
            else: return ReqResult.noperm
        elif d[1] == 'load':  # загрузить плагин
            if len(d) != 3: return ReqResult.broken
            if user == 'ROOT' and self.check_root():
                if self.load(d[2]): return ReqResult.good
                else: return ReqResult.none
            else: return ReqResult.noperm
        elif d[1] == 'unload':  # отгрузить плагин
            if len(d) != 3: return ReqResult.broken
            if user == 'ROOT' and self.check_root():
                if self.unload(d[2]): return ReqResult.good
                else: return ReqResult.none
            else: return ReqResult.noperm
        elif d[1] == 'reload':  # перезагрузить конфиг плагина
            if len(d) != 3: return ReqResult.broken
            if user == 'ROOT' and self.check_root():
                return self._reload(d[2])
            else: return ReqResult.noperm
        elif d[1] == 'get_version':  # получить версию панели
            with main.grlock:
                if main.config['SECURITY']['show_ver'] == 'yes' or (
                            main.config['SECURITY']['enable'] != 'yes'):
                    return self.MCPP_VERSION
                else: return ReqResult.none
        elif d[1] == 'stop_panel':  # остановить панель
            if user == 'ROOT' and self.check_root():
                try: return ReqResult.good
                finally: threading.Thread(target=self.stop_p).start()
            else: return ReqResult.noperm
        return ReqResult.noreply  # если ничего не вернулось до этой строчки

    def rec(self, user, data):
        """Обработка запросов от клиентов.

        :param user: имя пользователя
        :param data: зашифрованные данные в Base85
        :return: ответ от обработчика _com
        """
        d = None
        try:
            with main.grlock: main.debug.debug([user, data])
            if user == 'ROOT':  # супер-админ
                if not self.check_root():
                    self.wlog.error(json.dumps([user, ReqResult.disabled.name]))
                    return ReqResult.disabled
            else:  # пользователь
                with main.grlock:
                    if user not in self.users:
                        self.wlog.error(
                            json.dumps([user, ReqResult.notfound.name]))
                        return ReqResult.notfound
                    elif main.users[user]['enable'] != 'yes':
                        self.wlog.error(
                            json.dumps([user, ReqResult.disabled.name]))
                        return ReqResult.disabled
            # обработка массива данных
            try:
                dec = self.users[user].dec(data)
                with main.grlock:
                    main.debug.debug(json.dumps([user, data, dec]))
                if not dec: return ReqResult.wrongpass
                d = json.loads(dec)
            except BaseException as be:
                # данные не в JSON, либо другая ошибка
                with main.grlock:
                    main.debug.debug(
                        json.dumps(['except', user, data, str(be.args)]))
                self.wlog.critical(json.dumps([user, ReqResult.broken.name]))
                return ReqResult.broken
            if not d or len(d) <= 1: return ReqResult.broken
            if type(d) == str and d in ReqResult.__members__ and (
                ReqResult.__members__[d] == ReqResult.close):
                return ReqResult.close  # корректное завершение соединения
            if type(d) != list: return ReqResult.broken
            action = self._receive(user, d)
            if action in EventAction:
                with main.grlock:
                    main.debug.debug(json.dumps([action.name, user, data, d]))
                if action == EventAction.deny: return ReqResult.noreply
            else: d = action
            if d[0] == 'MCPP':
                # сообщение относится к базовым функциям панели
                try:
                    result = self._com(d, user)
                    if type(result) == ReqResult and result in ReqResult:
                        r = result.name
                        rr = True
                    else:
                        r = result
                        rr = False
                    with main.grlock:
                        main.debug.debug(json.dumps(['MCPP', user, data, d, r]))
                    self.wlog.info(json.dumps([user, d, r]))
                    if rr: return result
                    else: return json.dumps(['MCPP', r])
                except BaseException as be:
                    with main.grlock:
                        main.debug.debug(json.dumps(str(be.args)))
                    return ReqResult.error.name
            else:
                # сообщение относится к плагину
                if self.check_plugin(user, d[0]):
                    try:
                        result = self.__call(user, d)
                        if type(result) == ReqResult and result in ReqResult:
                            r = result.name
                            rr = True
                        else:
                            r = result
                            rr = False
                        with main.grlock:
                            main.debug.debug(
                                json.dumps(['plugin', user, data, d, r]))
                        self.wlog.info(json.dumps([user, d, r]))
                        if rr: return result
                        else: return json.dumps([d[0], r])
                    except BaseException as be:
                        with main.grlock:
                            main.debug.debug(json.dumps(str(be.args)))
                        return ReqResult.error.name
                else:
                    self.wlog.info(json.dumps([user, d, ReqResult.noperm.name]))
                    with main.grlock: main.debug.debug(
                        json.dumps(['plugin', user, data, d]))
                    return ReqResult.noperm
        except BaseException as be:
            self.wlog.exception(json.dumps([user, ReqResult.error.name]))
            with main.grlock:
                main.debug.debug(
                    json.dumps(['except', user, data, d, str(be.args)]))
            return ReqResult.error
