import __main__ as main


class Data:
    """Контейнер с данными."""
    def __init__(self, conf):
        """
        
        :param conf: главный конфиг ПУ
        """
        self.HOST = 'localhost'
        self.PORT = 25500
        self.LISTEN = 10
        self.enc = conf['MAIN']['encoding']
        self.rmax_try = int(conf['ROOT']['max_try'])
        self.max_try = int(conf['SECURITY']['max_try'])
        self.max_err = int(conf['SECURITY']['max_err'])
        self.max_login_err = int(conf['SECURITY']['max_login_err'])
        if conf['SECURITY']['enable'] == 'yes':
            self.sec_enable = True
        else:
            self.sec_enable = False
        self.rtint = float(conf['INTERNAL']['sock_remth_int'])
        self.ban_min = int(conf['SECURITY']['ban_mins'])
        self.to_auth = int(conf['SECURITY']['timeout_auth'])
        self.to = int(conf['SECURITY']['timeout'])
        self.wrong = {}
        """Пользователи, которые не правильно вводили пароль.

        ip - кол-во попыток."""
        self.err = {}
        """Пользователи, при авторизации которых возникли ошибки.

        ip - кол-во ошибок."""
        self.rerr = {}
        """Пользователи, при обработке данных от которых возникли ошибки.

        ip - кол-во ошибок."""
        self.ban = {}
        """Забаненные пользователи.

        ip - время (из time.time())."""
        self.conn_time = {}
        """Даты последнего приёма данных от сокетов.

        Ключ: (IP, port), выдаёт short (из time.time())."""
        self.auth_time = {}
        """Аналогично для тех, кто ожидает авторизации."""

    def clean_data(self):
        """Очистка всех словарей."""
        with main.grlock:
            main.debug.debug('clean_data')
        self.auth_time.clear()
        self.conn_time.clear()
        self.wrong.clear()
        self.err.clear()
        self.rerr.clear()
        self.ban.clear()

    def del_ip(self, ip):
        """ Удалить IP из словарей.

        :param ip: строковой IP
        """
        with main.grlock:
            main.debug.debug(str(ip))
        if ip in self.wrong:
            del self.wrong[ip]
        if ip in self.err:
            del self.err[ip]
        if ip in self.rerr:
            del self.rerr[ip]
