import socket
from mcpp.modules.mcpp_worker import *
from mcpp.modules.sub_server.server_data import *
import __main__ as main


class MCPPServerBase(MCPPWorker):
    """Базовый класс сервера на сокетах."""

    def __init__(self, mcppst):
        """

        :param mcppst: MCPPStarter для добавления в PluginAPI
        """
        MCPPWorker.__init__(self, self, mcppst)
        self._s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        """socket, используемый для принятия соединений."""
        self.th = []
        """Потоки, обрабатывающие клиентов."""
        self.exit_event = threading.Event()
        """Событие остановки сервера."""
        with main.grlock:
            self.data = Data(main.config)
            """Контейнер с данными."""
        self.slog = logging.getLogger('MCPPServer')
        """Логгер для записи в файл, если это включено."""
        self.slog.setLevel(logging.INFO)

    def _rec(self, conn, addr):
        """Обработчик клиента.

        :param conn: socket объект
        :param addr: (IP, порт)
        :return: Thread с recv, запущенный обработчик
        """
        with main.grlock:
            main.debug.debug(json.dumps(['start', addr]))

        def recv():
            """Обработчик клиента, запускаемый в новом потоке."""

            def wrong(text):
                """Обработка не верных попыток дешифровки.

                :param text: полученные данные
                :return: True в случае бана
                """
                with main.grlock:
                    main.debug.debug(str(text))
                if addr[0] in self.data.wrong:
                    self.data.wrong[addr[0]] += 1
                else:
                    self.data.wrong[addr[0]] = 1
                root = False
                try:
                    usr = json.loads(text)
                    if type(usr) == list and usr[0] == 'ROOT':
                        root = True
                except:
                    pass
                if root:
                    mtry = self.data.max_try
                else:
                    mtry = self.data.rmax_try
                if mtry != 0 and self.data.wrong[addr[0]] >= mtry:
                    action = self._ban(addr, BanReason.max_try)
                    if action == EventAction.deny:
                        with main.grlock:
                            main.debug.debug('deny')
                        return False
                    self.data.ban[addr[0]] = time.time()
                    with main.grlock:
                        main.debug.debug('True')
                    return True
                with main.grlock:
                    main.debug.debug('False')
                return False

            def error(auth, r):
                """Обработка ошибок выполнения запросов.

                :param auth: bool, авторизован ли пользователь
                :param r: значение ReqResult
                :return: True при бане
                """
                with main.grlock:
                    main.debug.debug(json.dumps([auth, r.name]))
                if auth:
                    # исключение не ошибок
                    if r == ReqResult.none:
                        return False
                    elif r == ReqResult.notfound:
                        return False
                    # проверка по словарю
                    if addr[0] in self.data.rerr:
                        self.data.rerr[addr[0]] += 1
                    else:
                        self.data.rerr[addr[0]] = 1
                    if self.data.rerr[addr[0]] >= self.data.max_err:
                        if self.data.max_err != 0:
                            action = self._ban(addr, BanReason.max_err)
                            if action == EventAction.deny: return False
                            self.data.ban[addr[0]] = time.time()
                            return True
                else:
                    if addr[0] in self.data.err:
                        self.data.err[addr[0]] += 1
                    else:
                        self.data.err[addr[0]] = 1
                    if self.data.err[addr[0]] >= self.data.max_login_err:
                        if self.data.max_login_err != 0:
                            action = self._ban(addr, BanReason.max_login_err)
                            if action == EventAction.deny: return False
                            self.data.ban[addr[0]] = time.time()
                            return True
                return False

            def send_req(req, user=None, cip=True):
                """Отправка значения ReqResult клиенту.

                :param req: значение ReqResult для отправки
                :param user: ник пользователя
                :param cip: bool, шифровать ли ответ
                """
                try:
                    with main.grlock:
                        main.debug.debug(json.dumps([req.name, user, cip]))
                    if cip:
                        if not user: return
                        conn.sendall(
                            self.enc(user, req.name).encode(self.data.enc))
                    else:
                        conn.sendall(req.name.encode(self.data.enc))
                except BaseException as be:
                    with main.grlock:
                        main.debug.debug(json.dumps(str(be.args)))

            # основная логика метода
            auth = False  # авторизован ли пользователь
            user = None  # ник пользователя
            try:
                while True:
                    with main.grlock:
                        # какими партиями получать данные
                        bs = int(main.config['INTERNAL']['recv_bytes'])
                        # сколько партий можно получать за раз
                        rr = int(main.config['INTERNAL']['recv_repeat'])
                    b = conn.recv(bs)  # первичное считывание
                    with main.grlock:
                        main.debug.debug(str(b))
                    with self.rlock:  # проверка на бан
                        if self.data.sec_enable and addr[0] in self.data.ban:
                            with main.grlock:
                                main.debug.debug(json.dumps(['ban', addr]))
                            break
                    if len(b) == bs:  # получение всех партий данных
                        ba = bytearray(b)
                        with main.grlock:
                            # лимит ожидания
                            tl = int(main.config['INTERNAL']['sock_recv_wait'])
                        i = 0
                        while True:
                            try:
                                with time_limit(tl):  # считывание
                                    bb = conn.recv(bs)
                                    with main.grlock:
                                        main.debug.debug(
                                            json.dumps(['bb', str(bb)]))
                                    ba += bb
                                    i += 1
                                    if rr != 0 and i >= rr: break
                                    if len(bb) != bs:
                                        break
                                    else:
                                        continue
                            except TimeoutError: break
                        b = ba
                    if b:  # обработка полученных данных
                        with main.grlock:
                            text = b.decode(main.config['MAIN']['encoding'])
                            main.debug.debug(text)
                        with self.rlock:
                            # если это пинг панели
                            if text == ReqResult.ping.name:
                                if self._ping(addr, user) == EventAction.deny:
                                    continue
                                send_req(ReqResult.pong, cip=False)
                                continue
                            # время последнего ответа используется для киков
                            if auth:
                                self.data.conn_time[addr] = time.time()
                            else:
                                self.data.auth_time[addr] = time.time()
                            # обработка запросов от авторизованных
                            if auth:
                                r = self.rec(user, text)
                                with main.grlock:
                                    main.debug.debug(str(r))
                                action = self._send(user, r)
                                if action in EventAction:
                                    if action == EventAction.deny:
                                        send_req(ReqResult.none, user)
                                        continue
                                else:
                                    r = action
                                if type(r) == ReqResult and r in ReqResult:
                                    # обработка ReqResult ответов от плагинов
                                    if r == ReqResult.noreply:
                                        send_req(r, user)
                                        continue
                                    elif r == ReqResult.close:
                                        break
                                    if self.data.sec_enable and (
                                                r != ReqResult.good):
                                        # обработка ошибок
                                        if r == ReqResult.wrongpass:
                                            if wrong(text):
                                                send_req(r, user)
                                                break
                                        else:
                                            if error(True, r):
                                                send_req(r, user)
                                                break
                                    with main.grlock:
                                        main.debug.debug(
                                            json.dumps([user, r.name]))
                                    send_req(r, user)
                                # обработка str ответов от плагинов
                                elif type(r) == str:
                                    with main.grlock:
                                        main.debug.debug(json.dumps([user, r]))
                                    conn.sendall(
                                        self.enc(user, r).encode(self.data.enc))
                                else:  # обработка ошибочных ответов
                                    with main.grlock:
                                        main.debug.debug(
                                            json.dumps(['not str', user]))
                                    send_req(ReqResult.error, user)
                            # авторизация
                            else:
                                r = self.login(text, addr)
                                with main.grlock:
                                    main.debug.debug(str(r))
                                if r[0] == ReqResult.close:
                                    break  # выход
                                elif r[0] == ReqResult.good:  # вход
                                    auth = True
                                    del self.data.auth_time[addr]
                                    user = r[1]
                                elif r[0] == ReqResult.logged:  # уже залогинен
                                    send_req(r[0], cip=False)
                                    break
                                if self.data.sec_enable and r[0] != ReqResult.good:
                                    # обработка ошибок
                                    if r[0] == ReqResult.wrongpass:
                                        if wrong(text):
                                            send_req(r[0], cip=False)
                                            break
                                    else:
                                        if error(False, r[0]):
                                            send_req(r[0], cip=False)
                                            break
                                send_req(r[0], cip=False)
                    else:
                        break
            except BaseException as be:
                with main.grlock:
                    main.debug.critical(json.dumps(str(be.args)))
                send_req(ReqResult.error, user, auth)
            finally:  # закрытие соединения, удаление данных и выход
                with main.grlock:
                    main.debug.debug(json.dumps(['exit', addr, user]))
                try:
                    send_req(ReqResult.close, user, auth)
                except:
                    with main.grlock:
                        main.debug.debug(json.dumps(['except', addr, user]))
                conn.close()
                with self.rlock:
                    # очистка данных
                    if not self.exit_event.is_set():
                        if addr in self._conn:
                            del self._conn[addr]
                        if addr in self.data.conn_time:
                            del self.data.conn_time[addr]
                        if user:
                            del self.names[user]
                            del self.users[user]
                        if addr in self.data.auth_time:
                            del self.data.auth_time[addr]
                        if t in self.th:
                            self.th.remove(t)
                with main.grlock:
                    mess = main.lang['LANG']['conn_close'] + ', ' + (
                        json.dumps([addr, user]))
                    self.slog.info(mess)
                    main.debug.info(mess)
                self._disconnect(user, addr)

        t = threading.Thread(target=recv, name=str(addr))
        t.start()
        return t
