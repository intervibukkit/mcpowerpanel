import threading
import time
from shutil import which
import __main__ as main
from mcpp.modules.mcpp_utils import get_time
from mcpp.modules.sub_container.container_base import *


class Container(ContainerBase):
    """Контейнер с сервером.

    Содержит методы взаимодействия с сервером: запуск, остановка, ввод комманд,
    список вывода консоли."""
    def __init__(self, serv=None, name=None):
        """

        :param serv: секция из конфига servers
        :param name: используется для создания процесса
        """
        ContainerBase.__init__(self, serv, name)
        if serv:
            self.run(serv, name)

    def put(self, mess):
        r"""Отправка данных в консоль сервера.

        Если нужно отправить несколько сток, следует использовать
        разделитель - \n.

        :param mess: строка
        """
        try:
            self.rlock.acquire()
            with main.grlock: main.debug.debug(mess)
            self._in.value = mess
            self._newin_event.set()
        finally: self.rlock.release()

    def chat(self, mess):
        """Отправка сообщения в чат.

        :param mess: строка
        """
        try:
            self.rlock.acquire()
            action = self._chat(mess)
            msg = mess
            if type(action) == EventAction and action == EventAction.deny:
                with main.grlock:
                    main.debug.debug(json.dumps(['deny', mess]))
                return
            elif type(action) == str: msg = action
            with main.grlock:
                main.debug.debug(json.dumps([mess, msg, action]))
            self._in.value = self.SERV['chat_command'].replace('$mess', msg)
            self._newin_event.set()
        finally: self.rlock.release()

    def wait_out(self):
        """Ожидание новой строки из консоли. Возвращает последнюю строку
        из вывода.

        После срабатывание метода, newout_event сбрасывается, самостоятельно
        изменять его значение не нужно."""
        self.newout_event.wait()
        try:
            self.rlock.acquire()
            mess = self.out[len(self.out)-1]
            with main.grlock: main.debug.debug(mess)
            return mess
        finally:
            self.newout_event.clear()
            self.rlock.release()

    def stop(self):
        """Отправка сигнала об остановке сервера."""
        try:
            self.rlock.acquire()
            with main.grlock: main.debug.debug('stop')
            self._exit_event.set()
        finally: self.rlock.release()

    def set_lim(self, lines):
        """Установить лимит значения out.

        :param lines: значение int
        """
        try:
            self.rlock.acquire()
            with main.grlock: main.debug.debug(json.dumps(lines))
            self._lim.value = int(lines)
        finally: self.rlock.release()

    def get_lim(self):
        """Получить лимит значения out (по-умолчанию = 50)

        :return: int
        """
        try:
            self.rlock.acquire()
            with main.grlock: main.debug.debug(str(self._lim.value))
            return self._lim.value
        finally: self.rlock.release()

    def _start(self, serv):
        """Запуск субпроцесса с сервером.

        Для запуска внутри обслуживающего процесса следует использовать run.

        :param serv: секция из конфига servers
        """
        try:
            try:
                self.rlock.acquire()
                self.stop_event.clear()
                self._exit_event.clear()
                path = main.THIS_PATH
                sp = serv['path']
                if sp != '.':  # точка - директория запуска ПУ
                    if sp[0] == '.': path = path + sp[1:]
                    else: path = sp
                self.dir_path.value = path
                if self._start_(serv, path) == EventAction.deny: return
                with main.grlock:
                    self.date_start.value = get_time()
                    if serv['log'] == 'yes':
                        handler = logging.FileHandler(
                            filename=os.path.join(path, serv['log_file']),
                            encoding=main.config['MAIN']['encoding'])
                        handler.setLevel(logging.INFO)
                        formatter = logging.Formatter(main.FORMAT)
                        handler.setFormatter(formatter)
                        self.log.addHandler(handler)
                    else: handler = None
                    main.debug.debug(str(path))
            finally: self.rlock.release()
            sub = None  # субпроцесс с сервером

            def exit_sub():
                """завершения субпроцесса (для MultiPopen)"""
                try:
                    sub.wait()
                    with main.grlock: main.debug.debug('exit_sub go')
                    try:
                        self.subproc.rlock.acquire()
                        self.subproc.returncode.value = sub.returncode
                        self.subproc.exit_event.set()
                        self.subproc.kill_event.set()
                        self._exit_sub()
                    finally: self.subproc.rlock.release()
                except BaseException as be:
                    with main.grlock:
                        main.debug.debug(json.dumps(str(be.args)))

            def kill_sub():
                """убийство субпроцесса по сигналу от MultiPopen"""
                try:
                    if not sub or sub.poll(): return
                    with main.grlock: main.debug.debug('kill_sub go')
                    try:
                        self.subproc.rlock.acquire()
                        if self._kill_sub() == EventAction.deny: return
                        if self.subproc._kill.value: sub.kill()
                        else: sub.terminate()
                    finally: self.subproc.rlock.release()
                except BaseException as be:
                    with main.grlock:
                        main.debug.debug(json.dumps(str(be.args)))

            def put(s, log=True):
                """добавление строки в out и в лог

                :param s: строка
                :param log: писать ли в лог
                """
                if not s: return
                try:
                    self.rlock.acquire()
                    if self._out(s) == EventAction.deny: return
                    while len(self.out) >= self._lim.value: self.out.pop(0)
                    self.out.append(s)
                    self.newout_event.set()
                    if log: self.log.info(s)
                finally: self.rlock.release()

            def get():
                """Запустить субпроцесс сервером и записать его в sub."""
                if self._start_server() == EventAction.deny: return
                with main.grlock: mess = main.lang['CONT']['start_serv']
                put(mess, False)
                self.log.warning(mess)
                pipe = subprocess.PIPE
                err = subprocess.STDOUT
                if serv['log_errors'] == 'yes':
                    with main.grlock:
                        err = open(os.path.join(path, serv['log_errors_file']),
                                   'a',
                                   encoding=main.config['MAIN']['encoding'])
                if serv['shell'] == 'yes':
                    sh = True
                    com = serv['run_command'].strip()
                else:
                    sh = False
                    com = serv['run_command'].strip().split(' ')

                def get_sub():
                    """Запускает субпроцесс, присваивая переменной sub.

                    :return: Popen
                    """
                    with main.grlock: main.debug.debug('get_sub')
                    nonlocal sub
                    sub = subprocess.Popen(com, shell=sh, stdin=pipe,
                                           stdout=pipe, stderr=err,
                                           close_fds=True, cwd=path,
                                           universal_newlines=True)
                    return sub

                # обработка команды запуска, которая позволяет указывать
                # как полный путь  к исполняемому файлу, так и просто
                # название программы
                try: get_sub()
                except FileNotFoundError as fne:
                    with main.grlock:
                        main.debug.debug(json.dumps(
                            ['FileNotFoundError', str(fne.args)]))
                    if not sh:
                        com[0] = which(cmd=com[0])
                        try: get_sub()
                        except BaseException as be:
                            with main.grlock:
                                main.debug.debug(json.dumps(
                                    ['except, sub = None', str(be.args)]))
                except BaseException as be:
                    with main.grlock:
                        main.debug.error(json.dumps(str(be.args)))
                finally:
                    if sub:  # заполнение subproc
                        try:
                            with main.grlock:
                                main.debug.debug('fill MultiPopen')
                            self.subproc.rlock.acquire()
                            self.subproc.clear()
                            rc = serv['run_command'].split(' ')
                            self.subproc.com.value = rc[0]
                            for a in rc[1:]: self.subproc.args.append(a)
                            self.subproc.pid.value = sub.pid
                            self.subproc.shell.value = sh
                        except BaseException as be:
                            with main.grlock:
                                main.debug.debug(json.dumps(str(be.args)))
                        finally: self.subproc.rlock.release()

            def wait():
                """Торможение запуска при перезаруске сервера."""
                i = int(0)
                w = int(serv['wait_sec_restart'])
                with main.grlock: main.debug.debug(str(w))
                try:
                    self.rlock.acquire()
                    action = self._wait(w)
                    if type(action) == EventAction and (
                        action == EventAction.deny): return
                    elif type(action) == int: w = action
                finally: self.rlock.release()
                while i < w:
                    try:
                        self.rlock.acquire()
                        if self._exit_event.is_set(): break
                    finally: self.rlock.release()
                    with main.grlock:
                        put(main.lang['CONT']['will_start'].replace(
                            '$sec', str(w-i)), False)
                    time.sleep(1)
                    i += 1
                with main.grlock: mess = main.lang['CONT']['start']
                put(mess, False)
                self.log.warning(mess)

            def in_in():
                """Обработка ввода."""
                try:
                    self.rlock.acquire()
                    if not self._newin_event.is_set(): return
                    if self._exit_event.is_set(): return
                    if sub.poll(): return
                    sub.stdin.write(self._in.value + '\n')
                    sub.stdin.flush()
                    self._put(self._in.value)
                    self._in.value = None
                    self._newin_event.clear()
                finally: self.rlock.release()

            def stop_serv():
                """Остановка сервера."""
                if not sub or sub.poll():
                    with main.grlock: main.debug.debug('subproc no alive')
                    exit_sub()
                    return
                try:
                    with main.grlock:
                        main.debug.debug('writing stop command')
                    sub.stdin.write(serv['stop_command'] + '\n')
                    sub.stdin.flush()
                except BrokenPipeError as bpe:
                    with main.grlock:
                        main.debug.debug(
                            json.dumps(['BrokenPipeError', str(bpe.args)]))
                finally:
                    to = int(serv['stop_sec_timeout'])
                    with main.grlock:
                        main.debug.debug(json.dumps(['wait', str(to)]))
                    try: sub.wait(timeout=to)
                    except subprocess.TimeoutExpired:
                        with main.grlock:
                            main.debug.debug('TimeoutExpired')
                        sub.kill()
                        with main.grlock: mess = main.lang['CONT']['kill']
                        put(mess, False)
                        self.log.critical(mess)
                    finally:
                        try:  # обработка вывода после остановки
                            with main.grlock:
                                main.debug.debug('communicate')
                            for text in sub.communicate(timeout=to):
                                if text:
                                    text = text.splitlines()
                                    for line in text:
                                        put(line.strip())
                        except BaseException as be:
                            with main.grlock:
                                main.debug.debug(json.dumps(
                                    ['except communicate', str(be.args)]))
                                mess = main.lang['CONT']['nocomm']
                            put(mess, False)
                            try:
                                self.rlock.acquire()
                                self.log.error(mess)
                            finally: self.rlock.release()
                        finally:
                            exit_sub()
                            with main.grlock:
                                mess = main.lang['CONT']['stopped']
                            put(mess, False)
                            try:
                                self.rlock.acquire()
                                self.date_stop = get_time()
                                self.log.warning(mess)
                            finally:
                                self.rlock.release()
                                try:
                                    if handler: handler.close()
                                finally: self._stop_server()

            def main_():
                """Главный цикл обработки данных."""
                try:
                    get()
                    while True:
                        if not sub or sub.poll():
                            with main.grlock:
                                main.debug.debug('subproc no alive, break')
                            break
                        in_in()
                        # КОСТЫЛИ
                        try:
                            self.rlock.acquire()
                            if self._exit_event.is_set():
                                # проверка на завершение
                                with main.grlock:
                                    main.debug.debug('_exit_event set')
                                break
                            if self.subproc.kill_event.is_set():
                                # убийство субпроцесса
                                kill_sub()
                                break
                        finally: self.rlock.release()
                        try:  # считывание данных из консоли
                            with main.grlock:
                                tl = int(
                                    main.config['INTERNAL']['cont_read_time'])
                            with time_limit(tl):  # чтобы цикл не зависал
                                s = sub.stdout.readline().strip()
                                sub.stdout.flush()
                        except TimeoutError: continue
                        # /КОСТЫЛИ
                        if s: put(s)  # запись вывода
                        elif serv['restart'] == 'yes':  # рестарт сервера
                            stop_serv()
                            with main.grlock:
                                main.debug.debug('restart')
                                mess = main.lang['CONT']['crash']
                            put(mess, False)
                            try:
                                self.rlock.acquire()
                                self.log.critical(mess)
                            finally: self.rlock.release()
                            wait()
                            get()
                            continue
                        else: break
                finally:  # остановка сервера
                    stop_serv()

            main_()

        except KeyboardInterrupt:
            with main.grlock: main.debug.debug('KeyboardInterrupt')
            stop_serv()
        finally:  # остановка потока обслуживающего процесса
            with main.grlock: main.debug.debug('_start end')
            try:
                self.rlock.acquire()
                self._stop()
                self.stop_event.set()
            finally: self.rlock.release()

    def run(self, serv, name=None):
        """Запуск субпроцесса с сервером, внутри обслуживающего процесса.

        :param serv: секция из конфига servers
        :param name: имя обслуживающего процесса
        """
        try:
            self.rlock.acquire()
            if self.proc and self.proc.is_alive():
                with main.grlock: main.debug.debug('proc alive')
                return
            if self._run(serv, name) == EventAction.deny: return
            with main.grlock: main.debug.debug(str(name))
            self.proc = multiprocessing.Process(target=self._start,
                                                args=(serv,), name=name)
            self.proc.start()
        finally: self.rlock.release()

    def wait_run(self, serv, name=None):
        """Запуск метода run после остановки сервера.

        :param serv: секция из конфига servers
        :param name: имя обслуживающего процесса
        """
        def waitr():
            with main.grlock: main.debug.debug('waiting stop_event')
            self.stop_event.wait()
            self.proc.join()
            self.run(serv, name)

        try:
            self.rlock.acquire()
            if self._wait_run(serv, name) == EventAction.deny: return
        finally: self.rlock.release()
        self.wait_thread = threading.Thread(target=waitr)
        self.wait_thread.start()
