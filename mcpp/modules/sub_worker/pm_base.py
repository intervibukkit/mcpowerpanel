import threading
import logging
import json
import __main__ as main
from mcpp.modules.mcpp_serv_manager import ServManager
from mcpp.modules.mcpp_plug_utils import PluginAPI
from mcpp.modules.mcpp_enums import *


class PluginManagerBase:
    """Базовый класс управления плагинами. Вызов событий."""
    def __init__(self, mcppw, mcpps, mcppst):
        """

        :param mcppw: MCPPWorker для добавления в PluginAPI.
        :param mcpps: MCPPServer для добавления в PluginAPI.
        :param mcppst: MCPPStarter для добавления в PluginAPI.
        """
        self.api = PluginAPI(self)  # заполняется по мере инициализации классов
        """Объект PluginAPI для доступа к элементам ПУ."""
        self.api.mcppworker = mcppw
        self.api.mcppserver = mcpps
        self.api.mcppstarter = mcppst
        self._plugins = {}
        """Загруженные плагины.

        название - Main класс."""
        self._pr = {
            EventsPriority.high: [],
            EventsPriority.medium: [],
            EventsPriority.low: []
        }
        """Приоритетные списки для обработки событий.

        приоритет - список с названиями плагинов."""
        with main.grlock:
            self.MCPP_VERSION = main.VERSION
            """Версия ПУ."""
        self.rlock = threading.Condition(threading.RLock())
        """Блокировка потока, Condition.

        Используется для обращения к элементам PluginAPI,
        MCPPWorker и MCPPServer."""
        self.sm = ServManager()
        """Контейнер с серверами."""
        self.log = logging.getLogger('PluginAPI')
        self.log.setLevel(logging.INFO)
        with main.grlock:
            if main.config['LOGS']['plugins'] == 'yes':
                enc = main.config['MAIN']['encoding']
                handler = logging.FileHandler(main.PATH_PLUGLOG,
                                              encoding=enc)
                handler.setLevel(logging.INFO)
                formatter = logging.Formatter(main.FORMAT_DEBUG)
                handler.setFormatter(formatter)
                self.log.addHandler(handler)

    def _load_plugin(self, plugin):
        """Вызов события загрузки плагина.

        :param plugin: имя плагина
        :return: значение EventAction
        """
        action = EventAction.none

        def load_(pls):
            try:
                nonlocal action
                for p in pls:
                    action = self._plugins[p].load_plugin(plugin, action)
            except BaseException as be:
                mess = json.dumps(str(be.args))
                with main.grlock:
                    main.debug.debug(mess)
                self.log.info(mess)

        for p in EventsPriority.__members__:
            load_(self._pr[EventsPriority.__getitem__(p)])
        if not action:
            action = EventAction.none
        with main.grlock:
            main.debug.debug(action.name)
        self.log.info(action.name)
        return action

    def _auth(self, user, addr, result):
        """Вызов события авторизации.

        :param user: ник пользователя
        :param addr: (IP, порт)
        :param result: первичное значение EventAction
        :return: значение EventAction
        """
        action = EventAction.none

        def auth_(pls):
            try:
                nonlocal action
                for p in pls:
                    action = self._plugins[p].auth(user, addr, result, action)
            except BaseException as be:
                mess = json.dumps(str(be.args))
                with main.grlock:
                    main.debug.debug(mess)
                self.log.error(json.dumps(str(be.args)))

        for p in EventsPriority.__members__:
            auth_(self._pr[EventsPriority.__getitem__(p)])
        if not action:
            action = EventAction.none
        mess = json.dumps([user, addr, result.name, action.name])
        with main.grlock:
            main.debug.debug(mess)
        self.log.info(mess)
        return action

    def _disconnect(self, user, addr):
        """Вызов события отключения.

        :param user: ник пользователя
        :param addr: (IP, порт)
        """
        with main.grlock:
            main.debug.debug(json.dumps([user, addr]))

        def disc(pls):
            try:
                for p in pls:
                    self._plugins[p].disconnect(user, addr)
            except BaseException as be:
                mess = json.dumps(str(be.args))
                with main.grlock:
                    main.debug.debug(mess)
                self.log.error(mess)

        for p in EventsPriority.__members__:
            disc(self._pr[EventsPriority.__getitem__(p)])

    def _connect(self, addr, result):
        """Вызов события подключения.

        :param addr: (IP, порт)
        :param result: первичное значение EventAction
        :return: значение EventAction
        """
        action = EventAction.none

        def con(pls):
            try:
                nonlocal action
                for p in pls:
                    action = self._plugins[p].connect(addr, result, action)
            except BaseException as be:
                mess = json.dumps(str(be.args))
                with main.grlock:
                    main.debug.debug(mess)
                self.log.error(mess)

        for p in EventsPriority.__members__:
            con(self._pr[EventsPriority.__getitem__(p)])
        if not action:
            action = EventAction.none
        mess = json.dumps([addr, result.name, action.name])
        with main.grlock:
            main.debug.debug(mess)
        self.log.info(mess)
        return action

    def _receive(self, user, data):
        """Вызов события принятия данных.

        :param user: ник пользователя
        :param data: данные в виде списка
        :return: значение EventAction или произвольные данные
        """
        action = EventAction.none

        def recv(pls):
            try:
                nonlocal action
                for p in pls:
                    action = self._plugins[p].receive(user, data, action)
            except BaseException as be:
                mess = json.dumps(str(be.args))
                with main.grlock:
                    main.debug.debug(mess)
                self.log.error(mess)

        for p in EventsPriority.__members__:
            recv(self._pr[EventsPriority.__getitem__(p)])
        if not action:
            action = EventAction.none
        if action in EventAction:
            ac = action.name
        else:
            ac = str(action)
        mess = json.dumps([user, str(data), ac])
        with main.grlock:
            main.debug.debug(mess)
        self.log.info(mess)
        return action

    def _send(self, user, data):
        """Вызов события отправки данных.

        :param user: ник пользователя
        :param data: данные в виде списка
        :return: значение EventAction или произвольные данные
        """
        action = EventAction.none

        def snd(pls):
            try:
                nonlocal action
                for p in pls:
                    action = self._plugins[p].send(user, data, action)
            except BaseException as be:
                mess = json.dumps(str(be.args))
                with main.grlock:
                    main.debug.debug(mess)
                self.log.error(mess)

        for p in EventsPriority.__members__:
            snd(self._pr[EventsPriority.__getitem__(p)])
        if not action:
            action = EventAction.none
        if action in EventAction:
            ac = action.name
        else:
            ac = str(action)
        mess = json.dumps([user, str(data), ac])
        with main.grlock:
            main.debug.debug(mess)
        self.log.info(mess)
        return action

    def _ban(self, addr, reason):
        """Вызов события бана пользователя.

        :param addr: (IP, порт)
        :param reason: значение BanReason
        :return: значение EventAction
        """
        action = EventAction.none

        def ban_(pls):
            try:
                nonlocal action
                for p in pls:
                    action = self._plugins[p].ban(addr, reason, action)
            except BaseException as be:
                mess = json.dumps(str(be.args))
                with main.grlock:
                    main.debug.debug(mess)
                self.log.error(mess)

        for p in EventsPriority.__members__:
            ban_(self._pr[EventsPriority.__getitem__(p)])
        if not action:
            action = EventAction.none
        mess = json.dumps([addr, reason.name, action.name])
        with main.grlock:
            main.debug.debug(mess)
        self.log.info(mess)
        return action

    def _unban(self, addr):
        """Вызов события разбана пользователя.

        :param addr: (IP, порт)
        :return: значение EventAction
        """
        action = EventAction.none

        def unb(pls):
            try:
                nonlocal action
                for p in pls:
                    action = self._plugins[p].unban(addr, action)
            except BaseException as be:
                mess = json.dumps(str(be.args))
                with main.grlock:
                    main.debug.debug(mess)
                self.log.error(mess)

        for p in EventsPriority.__members__:
            unb(self._pr[EventsPriority.__getitem__(p)])
        if not action:
            action = EventAction.none
        mess = json.dumps([addr, action.name])
        with main.grlock:
            main.debug.debug(mess)
        self.log.info(mess)
        return action

    def _kick(self, addr, reason):
        """Вызов события кика пользователя.

        :param addr: (IP, порт)
        :param reason: значение KickReason
        :return: значение EventAction
        """
        action = EventAction.none

        def kick_(pls):
            try:
                nonlocal action
                for p in pls:
                    action = self._plugins[p].kick(addr, reason, action)
            except BaseException as be:
                mess = json.dumps(str(be.args))
                with main.grlock:
                    main.debug.debug(mess)
                self.log.error(mess)

        for p in EventsPriority.__members__:
            kick_(self._pr[EventsPriority.__getitem__(p)])
        if not action:
            action = EventAction.none
        mess = json.dumps([addr, reason.name, action.name])
        with main.grlock:
            main.debug.debug(mess)
        self.log.info(mess)
        return action

    def _ping(self, addr, user):
        """Вызов события пинга панели.

        :param addr: (IP, порт)
        :param user: ник пользователя (None если не авторизован)
        :return: значение EventAction
        """
        action = EventAction.none

        def ping_(pls):
            try:
                nonlocal action
                for p in pls:
                    action = self._plugins[p].ping(addr, user, action)
            except BaseException as be:
                mess = json.dumps(str(be.args))
                with main.grlock:
                    main.debug.debug(mess)
                self.log.error(mess)

        for p in EventsPriority.__members__:
            ping_(self._pr[EventsPriority.__getitem__(p)])
        if not action:
            action = EventAction.none
        mess = json.dumps([addr, user])
        with main.grlock:
            main.debug.debug(mess)
        self.log.info(mess)
        return action

    def _stop_panel(self, reason):
        """Вызов события остановки панели.

        :param reason: значение StopReason
        """
        with main.grlock:
            main.debug.debug(json.dumps(str(reason)))

        def stop_(pls):
            try:
                for p in pls:
                    self._plugins[p].stop_panel(reason)
            except BaseException as be:
                mess = json.dumps(str(be.args))
                with main.grlock:
                    main.debug.debug(mess)
                self.log.error(mess)

        for p in EventsPriority.__members__:
            stop_(self._pr[EventsPriority.__getitem__(p)])

    def _start_panel(self, host, port, listen):
        """Вызов события старта панели.

        :param host: хост, с которого принимаются соединения
        :param port: порт
        :param listen: лимит кол-ва пользователей онлайн
        """
        with main.grlock:
            main.debug.debug(json.dumps([host, port, listen]))

        def start_(pls):
            try:
                for p in pls:
                    self._plugins[p].start_panel(host, port, listen)
            except BaseException as be:
                mess = json.dumps(str(be.args))
                with main.grlock:
                    main.debug.debug(mess)
                self.log.error(mess)

        for p in EventsPriority.__members__:
            start_(self._pr[EventsPriority.__getitem__(p)])
