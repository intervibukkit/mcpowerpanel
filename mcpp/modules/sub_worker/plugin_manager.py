import os
import sys
import __main__ as main
from mcpp.modules.sub_worker.pm_base import *


class PluginManager(PluginManagerBase):
    """Управление плагинами."""
    def __init__(self, mcppw, mcpps, mcppst):
        """

        :param mcppw: MCPPWorker для добавления в PluginAPI.
        :param mcpps: MCPPServer для добавления в PluginAPI.
        :param mcppst: MCPPStarter для добавления в PluginAPI.
        """
        PluginManagerBase.__init__(self, mcppw, mcpps, mcppst)

    def add_sm_plugin(self, plugin):
        """Добавить ServPlugin в словарь ServManager для прослушки событий.

        :param plugin: объект плагина
        :return: True в случае успеха
        """
        result = False
        if plugin not in self.sm.pls[plugin.priority]:
            self.sm.pls[plugin.priority].append(plugin)
            result = True
        with main.grlock:
            main.debug.debug(json.dumps([plugin.NAME, result]))
        return result

    def del_sm_plugin(self, plugin):
        """Удалить плагин из словаля ServManager.

        :param plugin: объект плагина
        :return: True в случает успеха
        """
        result = False
        if plugin in self.sm.pls[plugin.priority]:
            self.sm.pls[plugin.priority].remove(plugin)
            result = True
        with main.grlock:
            main.debug.debug(json.dumps([plugin.NAME, result]))
        return result

    def add_serv_plugin(self, plugin, server):
        """Добавить ContainerPlugin в словарь плагинов сервера для прослушки
        событий.

        :param plugin: объект плагина
        :param server: имя сервера, который есть в servs из ServManager
        :return: True в случае успеха
        """
        result = False
        if server in self.sm.servs and (
                    plugin not in self.sm.servs[server].pls[plugin.priority]):
            try:
                self.sm.servs[server].rlock.acquire()
                self.sm.servs[server].pls[plugin.priority].append(plugin)
                result = True
            finally:
                self.sm.servs[server].rlock.release()
        with main.grlock:
            main.debug.debug(json.dumps([plugin.NAME, server, result]))
        return result

    def del_serv_plugin(self, plugin, server):
        """Удалить плагин из словаря сервера.

        :param plugin: объект плагина
        :param server: имя сервера
        :return: True в случае успеха
        """
        result = False
        if server in self.sm.servs and (
                    plugin in self.sm.servs[server].pls[plugin.priority]):
            try:
                self.sm.servs[server].rlock.acquire()
                self.sm.servs[server].pls[plugin.priority].remove(plugin)
                result = True
            finally:
                self.sm.servs[server].rlock.release()
        with main.grlock:
            main.debug.debug(json.dumps([plugin.NAME, server, result]))
        return result

    def load(self, plugin=None):
        """Загрузить все или конкретный плагин.

        :param plugin: имя плагина
        :return: True в случае успеха
        """
        with main.grlock:
            if main.config['MAIN']['plugins'] != 'yes':
                mess = main.lang['PLUG']['plug_dis'] + ', ' + str(plugin)
                main.debug.warning(mess)
                self.log.warning(mess)
                return
            pp = main.PATH_PLUGINS
        pd = os.listdir(pp)
        sys.path.append(pp)
        mess = json.dumps([plugin, pp])
        with main.grlock:
            main.debug.debug(mess)
        self.log.info(json.dumps([plugin, pp]))

        def load_(p):
            if p[-3:] != '.py': return False
            try:
                plugin = __import__(os.path.splitext(p)[0])
                m = plugin.Main(self.api)
                a = m.load(self.api)
                with main.grlock:
                    mess = main.lang['PLUG']['load_plugin'].replace(
                        '$plug', p)
                    self.log.warning(mess)
                    main.debug.info(mess)
                    if type(a) == EventAction:
                        main.debug.debug(a.name)
                if a == EventAction.deny: return False
                if m.NAME in self._plugins:
                    mess = json.dumps(['duplicate, unload', m.NAME])
                    with main.grlock: main.debug.debug(mess)
                    self.log.error(mess)
                    m.unload()
                    return False
                a = self._load_plugin(m.NAME)
                if a == EventAction.deny: return False
                self._plugins[m.NAME] = m
                self._pr[m.priority].append(m.NAME)
                with main.grlock:
                    mess = main.lang['PLUG']['add_plugin'].replace(
                        '$plug', m.NAME)
                    self.log.warning(mess)
                    main.debug.info(mess)
                return True
            except BaseException as be:
                mess = json.dumps(str(be.args))
                with main.grlock:
                    main.debug.error(mess)
                self.log.error(mess)

        result = False
        if plugin:
            if plugin not in pd: return False
            result = load_(plugin)
        else:
            for p in pd:
                res = load_(p)
                if not result: result = res
        return result

    def unload(self, plugin=None):
        """Отгрузить все или конкретный плагин.

        :param plugin: имя плагина
        :return: True в случае успеха
        """
        with main.grlock:
            main.debug.debug(json.dumps(plugin))

        def unload_(p):
            try:
                if p not in self._plugins: return False
                self._plugins[p].unload()
                self._pr[self._plugins[p].priority].remove(p)
                if plugin: del self._plugins[p]
                with main.grlock:
                    mess = main.lang['PLUG']['unload_plugin'].replace(
                        '$plug', p)
                    self.log.warning(mess)
                    main.debug.info(mess)
                return True
            except BaseException as be:
                mess = json.dumps(str(be.args))
                with main.grlock:
                    main.debug.error(mess)
                self.log.error(mess)

        result = False
        if plugin:
            result = unload_(plugin)
        else:
            for p in self._plugins:
                res = unload_(p)
                if not result: result = res
            self._plugins.clear()
        return result

    def __set_priority__(self, pri, plugin):
        """Изменить приоритет вызова событий плагину.

        pri - EventPriority, plugin - название плагина"""
        with main.grlock: main.debug.debug(json.dumps([pri.name, plugin]))
        if plugin not in self._plugins:
            mess = 'not in _plugins'
            with main.grlock: main.debug.debug(mess)
            self.log.error(mess)
            return
        self._pr[self._plugins[plugin].priority].remove(plugin)
        self._pr[pri].append(plugin)

    def __call(self, user, data):
        """Вызвать плагин.

        :param user: ник пользователя
        :param data: данные в виде списка
        :return: см. док. client-server, если плагин не найден -
        ReqResult.notfound
        """
        if type(data) != list or len(data) < 2: return ReqResult.broken
        with main.grlock:
            main.debug.debug(json.dumps([user, data]))
        if data[0] in self._plugins:
            self.log.info(json.dumps(['call ' + data[0], user, data]))
            return self._plugins[data[0]].call(user, data)
        else:
            return ReqResult.notplugin

    def _func(self, plugin):
        """Получить функции плагина.

        :param plugin: имя плагина
        :return: см. док. client-server
        """
        result = None
        if plugin in self._plugins: result = self._plugins[plugin].func()
        mess = json.dumps([plugin, str(result)])
        with main.grlock: main.debug.debug(mess)
        self.log.info(mess)
        return result

    def _reload(self, plugin):
        """Перезагрузить конфиг плагина (если поддерживается).

        :param plugin: имя плагина
        :return: значение ReqResult
        """
        if plugin in self._plugins:
            result = self._plugins[plugin].reload()
        else:
            result = ReqResult.notfound
        if result:
            # костыль от странной магии возвращения value
            # вместо всего значения ReqResult
            for r in ReqResult.__members__:
                if result == ReqResult.__members__[r].value:
                    result = ReqResult.__members__[r]
                    break
        else:
            result = ReqResult.error
        mess = json.dumps([plugin, result.name])
        with main.grlock:
            main.debug.debug(mess)
        self.log.info(mess)
        return result
