import socket
import __main__ as main
from mcpp.modules.sub_worker.plugin_manager import *


class MCPPWorkerBase(PluginManager):
    def __init__(self, mcpps, mcppst):
        """

        :param mcpps: MCPPServer для добавления в PluginAPI.
        :param mcppst: MCPPStarter для добавления в PluginAPI.
        """
        PluginManager.__init__(self, self, mcpps, mcppst)
        self.users = {}
        """Авторизованные пользователи.

        Ключ - ник, содержимое - AESCip."""
        self._conn = {}
        """Сокеты подключенных пользователей.

        Ключ: (IP, port)"""
        self.names = {}
        """ник - (IP, port)"""
        self._p_stop_event = threading.Event()
        """Событие, останавливающее панель."""
        self.wlog = logging.getLogger('MCPPWorker')
        """Логгер для записи в файл, если это включено."""
        self.wlog.setLevel(logging.INFO)
        with main.grlock:
            if main.config['LOGS']['worker'] == 'yes':
                enc = main.config['MAIN']['encoding']
                handler = logging.FileHandler(main.PATH_WORKLOG,
                                              encoding=enc)
                handler.setLevel(logging.INFO)
                formatter = logging.Formatter(main.FORMAT)
                handler.setFormatter(formatter)
                self.wlog.addHandler(handler)

    def check_root(self):
        """Включён ли ROOT.

        :return: True если да
        """
        with main.grlock:
            if main.config['ROOT']['enable'] == 'yes': result = True
            else: result = False
            main.debug.debug(str(result))
            return result

    def check_user(self, user):
        """Есть ли пользователь в списке и включён ли он.

        :param user: имя пользователя
        :return: True если да
        """
        if user == 'ROOT' and self.check_root(): return True
        with main.grlock:
            if user in main.users:
                if main.users[user]['enable'] != 'yes': result = False
                else: result = True
            else: result = False
            main.debug.debug(json.dumps([user, result]))
            return result

    def check_perm(self, user, perm):
        """Есть ли у пользователя разрешение и включено ли оно.

        :param user: имя пользователя
        :param perm: имя разрешения
        :return: True если да
        """
        with main.grlock:
            main.debug.debug(json.dumps([user, perm]))
        if not self.check_user(user): return False
        if user == 'ROOT' and self.check_root(): return True
        with main.grlock:
            g = main.users[user]['group']
            if g not in main.groups: return False
            elif main.groups[g]['enable'] != 'yes': return False
            elif perm in main.groups[g] and main.groups[g][perm] == 'yes':
                return True
            else: return False

    def get_perm(self, user, perm):
        """Получить значение разрешения.

        :param user: имя пользователя
        :param perm: разрешение
        :return: значение, если user - ROOT, вернёт "ROOT", если группа
        не найдена или выключена - None.
        """
        with main.grlock:
            main.debug.debug(json.dumps([user, perm]))
        if not self.check_user(user): return None
        if user == 'ROOT' and self.check_root(): return 'ROOT'
        with main.grlock:
            g = main.users[user]['group']
            if g not in main.groups: return None
            elif main.groups[g]['enable'] != 'yes': return None
            else: return main.groups[g][perm]

    def check_plugin(self, user, plugin):
        """Проверить наличие разрешения на доступ к плагину.

        :param user: имя пользователя
        :param plugin: имя плагина
        :return: True если доступ есть
        """
        with main.grlock:
            main.debug.debug(json.dumps([user, plugin]))
        perm = self.get_perm(user, 'plugins')
        if perm == 'ROOT': return True
        perm = perm.split('|')
        if plugin in perm: return True
        else: return False

    def check_serv(self, user, serv):
        """Проверить доступ к серверу.

        :param user: имя пользователя
        :param serv: имя сервера
        :return: True если доступ есть
        """
        with main.grlock:
            main.debug.debug(json.dumps([user, serv]))
        if not self.check_user(user): return False
        if user == 'ROOT' and self.check_root(): return True
        with main.grlock:
            g = main.users[user]['group']
            if g not in main.groups: return False
            elif main.groups[g]['enable'] != 'yes': return False
            s = main.groups[g]['main.servers']
        if s == '.': return True
        elif s == '-': return False
        s = s.split('|')
        if serv in s: return True
        else: return False

    def enc(self, user, data):
        """Зашифровать данные.

        :param user: имя пользователя
        :param data: данные в виде списка со строками
        :return: зашифрованная строка в Base85, или значение ReqResult
        в случае ошибки
        """
        with main.grlock:
            main.debug.debug(json.dumps([user, data]))
        if user not in self.users: return ReqResult.notfound
        try:
            if type(data) == str and data in ReqResult.__members__: d = data
            else: d = json.dumps(data)
            return self.users[user].enc(d)
        except ValueError: return ReqResult.broken

    def list_(self, user, run):
        """Получить список запущенных или остановленных серверов.

        :param user: имя пользователя
        :param run: bool, True - выдать запущенные, False - остановленные
        :return: список серверов, либо значение ReqResult
        """
        if self.check_perm(user, 'stats'):
            if self.get_perm(user, 'main.servers') == '-':
                return ReqResult.noperm
            result = []
            for s in self.sm.servs:
                if not self.check_serv(user, s): continue
                serv = self.sm.servs[s]
                try:
                    serv.rlock.acquire()
                    if serv.stop_event.is_set():
                        if run: continue
                    else:
                        if not run: continue
                    result.append(s)
                finally:
                    serv.rlock.release()
            if not result: return ReqResult.notfound
            return result
        else:
            return ReqResult.noperm

    def date_(self, user, run):
        """Получить даты запуска или остановки серверов.

        :param user: имя пользователя
        :param run: bool, True - даты запущенных серверов,
        False - остановленных
        :return: словарь, где ключами служат имена серверов, а значениями
        даты в строковом формате (или None), может вернуть значение ReqResult
        """
        if self.check_perm(user, 'stats'):
            if self.get_perm(user, 'main.servers') == '-':
                return ReqResult.noperm
            result = {}
            for s in self.sm.servs:
                if not self.check_serv(user, s): continue
                serv = self.sm.servs[s]
                try:
                    serv.rlock.acquire()
                    if run:
                        result[s] = serv.date_start.value
                    else:
                        result[s] = serv.date_stop.value
                finally:
                    serv.rlock.release()
            if not result: return ReqResult.notfound
            return result
        else:
            return ReqResult.noperm

    def date_serv(self, user, run, d):
        """Получить дату запуска или остановки сервера.

        :param user: имя пользователя
        :param run: bool, True - дата запуска, False - остановки
        :param d: данные в виде списка строк
        :return: дата в строковом формате, None или значение ReqResult
        """
        if len(d) != 3: return ReqResult.broken
        if self.check_perm(user, 'stats'):
            if d[2] not in self.sm.servs: return ReqResult.notfound
            if not self.check_serv(user, d[2]): return ReqResult.noperm
            serv = self.sm.servs[d[2]]
            try:
                serv.rlock.acquire()
                if run:
                    return serv.date_start.value
                else:
                    return serv.date_stop.value
            finally:
                serv.rlock.release()
        else:
            return ReqResult.noperm

    def start_stop(self, user, run, name=None):
        """Запустить или остановить сервер(а).

        :param user: имя пользователя
        :param run: bool, True - запустить, False - остановить
        :param name: имя сервера (при None - все сервера)
        :return: список имён серверов или значение ReqResult
        """
        if self.check_perm(user, 'control'):
            result = []
            if name:
                if not run and name not in self.sm.servs:
                    return ReqResult.none
                elif not self.check_serv(user, name):
                    return ReqResult.noperm
                if run:
                    res = self.sm.start(name)
                else:
                    res = self.sm.stop(name)
                if res: result.append(name)
            else:
                if self.get_perm(user, 'main.servers') == '-':
                    return ReqResult.noperm
                for s in self.sm.servs:
                    if not self.check_serv(user, s): continue
                    if run:
                        res = self.sm.start(s)
                    else:
                        res = self.sm.stop(s)
                    if res: result.append(s)
            if not result: return ReqResult.notfound
            return result
        else:
            return ReqResult.noperm

    def kill(self, user, name=None):
        """Убить процесс(ы) сервера(ов).

        :param user: имя пользователя
        :param name: имя сервера (при None - все сервера)
        :return: список имён серверов или значение ReqResult
        """
        if self.check_perm(user, 'control'):
            result = []
            if name:
                if name not in self.sm.servs:
                    return ReqResult.none
                elif not self.check_serv(user, name):
                    return ReqResult.noperm
                if self.sm.kill(name): result.append(name)
            else:
                if self.get_perm(user, 'main.servers') == '-':
                    return ReqResult.noperm
                for s in self.sm.servs:
                    if not self.check_serv(user, s): continue
                    if self.sm.kill(s): result.append(s)
            if not result: return ReqResult.notfound
            return result
        else:
            return ReqResult.noperm

    def restart(self, user, name=None):
        """Перезапустить сервер(а).

        :param user: имя пользователя
        :param name: имя сервера (при None - все сервера)
        :return: список имён серверов или значение ReqResult
        """
        if self.check_perm(user, 'control'):
            s = self.get_perm(user, 'main.servers')
            if not s:
                return ReqResult.none
            elif s == '-':
                return ReqResult.noperm
            if name:
                sl = [name]
            else:
                sl = self.sm.servs.keys()
            result = []
            for serv in sl:
                if serv == 'DEFAULT': continue
                if not self.check_serv(user, serv): continue
                s = self.sm.servs[serv]
                try:
                    s.rlock.acquire()
                    s.stop()
                    s.wait_run(s.SERV, s.name.value)
                finally:
                    s.rlock.release()
                result.append(serv)
            if not result: return ReqResult.notfound
            return result
        else:
            return ReqResult.noperm

    def get_pid(self, user, name=None, sub=True):
        """Получить PID(ы) субпроцесса(ов).

        :param user: имя пользователя
        :param name: имя сервера (при None - все сервера)
        :param sub: bool, True - PID(ы) субпроцессов, False - PID(ы)
        обслуживающих процессов
        :return: словарь, где ключами служат имена серверов
        """
        if self.check_perm(user, 'control'):
            s = self.get_perm(user, 'main.servers')
            if not s:
                return ReqResult.none
            elif s == '-':
                return ReqResult.noperm
            if name:
                sl = [name]
            else:
                with main.grlock:
                    sl = main.servers.keys()
            result = {}
            for serv in sl:
                if serv not in self.sm.servs: continue
                if not self.check_serv(user, serv): continue
                con = self.sm.servs[serv]
                if sub:
                    if con.subproc.pid.value:
                        result[serv] = con.subproc.pid.value
                    elif len(sl) == 1:
                        return ReqResult.none
                else:
                    if con.proc:
                        result[serv] = con.proc.pid
                    elif len(sl) == 1:
                        return ReqResult.none
            if not result: return ReqResult.notfound
            return result
        else:
            return ReqResult.noperm

    def enable_user(self, user, d, en=False):
        """Включить или выключить пользователя.

        :param user: имя пользователя
        :param d: данные в виде списка строк
        :param en: bool, True - включить, False - выключить
        :return: значение ReqResult
        """
        if len(d) != 3: return ReqResult.broken
        if user == 'ROOT' and self.check_root():
            with main.grlock:
                if d[2] not in main.users: return ReqResult.notfound
                if en:
                    main.users[d[2]]['enable'] = 'yes'
                else:
                    main.users[d[2]]['enable'] = 'no'
                    if d[2] in self.names:
                        self._conn[self.names[d[2]]].shutdown(
                            socket.SHUT_RDWR)
                with open(main.PATH_USERS, 'w') as users_:
                    main.users.write(users_)
                return ReqResult.good
        else:
            return ReqResult.noperm

    def enable_perm(self, user, d, en=True):
        """Включить или выключить разрешение у группы.

        :param user: имя пользователя
        :param d: данные в виде списка строк
        :param en: bool, True - включить, False - выключить
        :return: значение ReqResult
        """
        if len(d) != 4: return ReqResult.broken
        if user == 'ROOT' and self.check_root():
            with main.grlock:
                if d[2] not in main.groups: return ReqResult.notfound
                if d[3] not in main.groups[d[2]]: return ReqResult.notfound
                if en:
                    main.groups[d[2]][d[3]] = 'yes'
                else:
                    main.groups[d[2]][d[3]] = 'no'
                return ReqResult.good
        else:
            return ReqResult.noperm

    def enable_group(self, user, d, en=True):
        """Включить или выключить группу.

        :param user: имя пользователя
        :param d: данные в виде списка строк
        :param en: bool, True - включить, False - выключить
        :return: значение ReqResult
        """
        if len(d) != 3: return ReqResult.broken
        if user == 'ROOT' and self.check_root():
            with main.grlock:
                if d[2] not in main.groups: return ReqResult.notfound
                if en:
                    main.groups[d[2]]['enable'] = 'yes'
                else:
                    main.groups[d[2]]['enable'] = 'no'
                    for n in self.names:
                        if main.users[n]['group'] == d[2]:
                            self._conn[self.names[n]].shutdown(
                                socket.SHUT_RDWR)
                with open(main.PATH_GROUPS, 'w') as g:
                    main.groups.write(g)
                return ReqResult.good
        else:
            return ReqResult.noperm

    def stop_p(self):
        """Остановка панели."""
        with self.rlock:
            with main.grlock:
                main.debug.debug('stop_p go')
            self._stop_panel(StopReason.command)
            self._p_stop_event.set()
