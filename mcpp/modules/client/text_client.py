import __main__ as main
from mcpp.modules.mcpp_enums import ReqResult
from mcpp.modules.client.sock_client import SockClient
from mcpp.modules.client.sub_text.text_base import *


class TextClient(TextClientBase):
    def __init__(self):
        TextClientBase.__init__(self)
        self.th = threading.Thread(target=self.__recv)

    def auth(self):
        a = self.sc.auth()
        if a == ReqResult.good:
            main.logger.critical(main.lang['TEXT']['good'])
            return True
        elif a == ReqResult.broken: mess = main.lang['TEXT']['broken']
        elif a == ReqResult.badhost: mess = main.lang['TEXT']['badhost']
        elif a == ReqResult.disabled: mess = main.lang['TEXT']['disabled']
        elif a == ReqResult.notfound: mess = main.lang['TEXT']['notfound']
        elif a == ReqResult.none: mess = main.lang['TEXT']['none']
        elif a == ReqResult.wrongpass: mess = main.lang['TEXT']['wrongpass']
        elif a == ReqResult.error: mess = main.lang['TEXT']['error']
        elif a == ReqResult.full: mess = main.lang['TEXT']['full']
        elif a == ReqResult.banned: mess = main.lang['TEXT']['banned']
        elif a == ReqResult.logged: mess = main.lang['TEXT']['logged']
        else:
            mess = main.lang['TEXT']['other'].replace('$err', a.name)
        main.logger.warning(mess)
        return False

    def stop(self):
        pass

    def start(self, host=None, port=None):
        i = 0
        while True:
            if not self.name: self.name = input(main.lang['TEXT']['iname'] + ': ')
            if not self.passwd:
                self.passwd = input(main.lang['TEXT']['ipasswd'] + ': ')
            i += 1
            if i >= 3: break
        if not self.name or self.passwd: return False
        if not host: self.host = main.config['MAIN']['host']
        else:
            self.host = host
            main.config['MAIN']['host'] = host
            with open(main.PATH_CONF, 'w') as main.config_file:
                main.config.write(main.config_file)
        if not port:
            self.port = int(main.config['MAIN']['port'])
        else:
            self.port = port
            main.config['MAIN']['port'] = port
            with open(main.PATH_CONF, 'w') as main.config_file:
                main.config.write(main.config_file)
        try:
            self.sc = SockClient(
                main.config['MAIN']['encoding'], self.name, self.passwd)
            return self.sc.start(self.host, self.port)
        except BaseException as be:
            main.logger.debug(json.dumps(str(be.args)))
            return False

    def __recv(self):
        pass
