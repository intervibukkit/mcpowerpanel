import __main__ as main
from mcpp.modules.client.sock_client import *


class DebugMode:
    """Отладочный клиент, все запросы вводятся вручную,
    а ответы отображаются в исходном виде."""
    def __init__(self):
        self.sc = None
        """SockClient."""

    def work(self):
        """Метод, из которого происходит основное взаиможействие."""
        with main.grlock:
            main.logger.info(main.lang['DEBUG']['exit_com'])
        try:
            while True:
                text = input('> ')
                if not text or not text.strip(): continue
                with main.grlock:
                    main.logger.debug(text)
                if text == '/exit': break
                with self.sc.rlock:
                    if text == 'ping': cip = False
                    else: cip = True
                    if not self.sc.send(text, cip): break
                with self.sc.rlock: a = self.sc.recv()
                if a:
                    with main.grlock:
                        main.logger.info('answer: ' + a)
                    if a == '"close"': break
                else:
                    with main.grlock:
                        main.logger.info('answer None')
        finally:
            with self.sc.rlock: self.sc.stop()

    def start(self):
        """Ввод данных и подключение."""
        with main.grlock:
            host = input(main.lang['DEBUG']['ihost'] + ': ')
            port = input(main.lang['DEBUG']['iport'] + ': ')
            enc = input(main.lang['DEBUG']['ienc'] + ': ')
            name = input(main.lang['DEBUG']['iname'] + ': ')
            passwd = input(main.lang['DEBUG']['ipasswd'] + ': ')
        if not host or len(host.strip()) == 0: host = 'localhost'
        if not port or len(port.strip()) == 0: port = 25500
        else: port = int(port)
        if not enc or len(enc.strip()) == 0: enc = 'utf-8'
        if not name or len(name.strip()) == 0: name = 'none'
        if not passwd or len(passwd.strip()) == 0: passwd = 'none'
        main.logger.debug(json.dumps([host, port, enc, name, passwd]))
        self.sc = SockClient(enc, name, passwd)
        if not self.sc.start(host, port): return
        a = self.sc.auth()
        if a:
            main.logger.info(a.name)
            if a != ReqResult.good: return
        else:
            main.logger.error('auth None')
            return
        self.work()