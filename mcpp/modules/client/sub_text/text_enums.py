from enum import Enum, IntEnum


class ControlCom(Enum):
    """Команды управления клиентом."""
    logout = '/logout'
    list_ = '/list'
    list_run = '/list_run'
    list_stop = '/list_stop'
    status = '/status'
    dstart_all = '/dstart_all'
    dstop_all = '/dstop_all'
    dstart = '/dstart'
    dstop = '/dstop'
    start_all = '/start_all'
    stop_all = '/stop_all'
    start = '/start'
    stop = '/stop'
    kill_all = '/kill_all'
    kill = '/kill'
    out = '/out'
    chat = '/chat'
    chat_all = '/chat_all'
    input_ = '/com'
    input_all = '/com_all'
    rest_all = '/rest_all'
    restart = '/restart'
    lim = '/lim'
    setlim = '/setlim'
    spids = '/spids'
    spid = '/spid'
    ppids = '/ppids'
    ppid = '/ppid'
    users = '/users'
    kick = '/kick'
    duser = '/duser'
    euser = '/euser'
    npass = '/npass'
    npassu = '/npassu'
    reg = '/reg'
    sgroup = '/sgroup'
    delu = '/delu'
    aperm = '/aperm'
    dperm = '/dperm'
    enperm = '/enperm'
    diperm = '/diperm'
    agroup = '/agroup'
    engroup = '/engroup'
    digroup = '/digroup'
    reload = '/reload'
    plugins = '/plugins'
    pinfo = '/pinfo'
    load_all = '/load_all'
    unload_all = '/unload_all'
    load = '/load'
    unload = '/unload'
    preload = '/preload'
    ver = '/ver'
    ping = '/ping'
    stop_panel = '/pstop'
    server = '/server'
    help = '/help'
    null = 'null'


class ServerCom(Enum):
    """Команды управления игровым сервером."""
    exit_ = '/exit'
    help = '/help'
    null = 'null'


class ComResult(IntEnum):
    """Результат выполнения команды (для com_worker)."""
    logout = 0
    server = 1
    control = 2
    error = 3
