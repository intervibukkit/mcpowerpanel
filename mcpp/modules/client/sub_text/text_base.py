import json
import threading
import __main__ as main
from mcpp.modules.client.sub_text.text_enums import *


class TextClientBase:
    def __init__(self):
        self.sc = None
        self.name = None
        self.passwd = None
        self.host = None
        self.port = None
        self.__server = None

    def get_com(self, com_enum, text):
        arr = text.split(' ')
        cmd = com_enum.null
        for e in com_enum.__members__:
            if com_enum.__members__[e].value == arr[0]:
                cmd = com_enum.__members__[e]
                break
        with main.grlock:
            main.logger.debug(json.dumps([text, cmd.name, arr]))
        return cmd, arr[1:], text

    def com_worker(self, com_func, com_enum):
        def worker():
            while True:
                text = input('> ')
                if not text or not text.strip(): continue
                with main.grlock:
                    main.logger.debug(text)
                try:
                    result = com_func(self.get_com(com_enum, text))
                    if result: return result
                except BaseException as be:
                    with main.grlock:
                        main.logger.error(json.dumps(str(be.args)))
                    break
            return ComResult.error

        try:
            while True:
                result = worker()
                if not result:
                    break
                if result == ComResult.server:
                    com_func = self.server()
                elif result == ComResult.control:
                    com_func = self.mcpp()
                elif result == ComResult.logout:
                    break
                elif result == ComResult.error:
                    break
        except BaseException as be:
            with main.grlock:
                main.logger.error(json.dumps(str(be.args)))

    def send(self, com, args=(), length=1):
        if len(args) == length:
            arr = ['MCPP', com]
            for a in args: arr.append(a)
            self.sc.self.send(json.dumps(arr))
        else:
            with main.grlock:
                main.logger.warning(main.lang['TEXT']['noargs'])

    def send_one(self, text):
        self.sc.self.send(json.dumps(['MCPP', text]))

    def mcpp(self):
        def com(cmd):
            def help():
                pass

            if cmd[0] == ControlCom.logout:
                with main.grlock:
                    main.logger.warning(main.lang['TEXT']['exit_mess'])
                return True
            elif cmd[0] == ControlCom.list_:
                self.send_one('list')
            elif cmd[0] == ControlCom.list_run:
                self.send_one('list_run')
            elif cmd[0] == ControlCom.list_stop:
                self.send_one('list_stop')
            elif cmd[0] == ControlCom.status:
                self.send('status', cmd[1])
            elif cmd[0] == ControlCom.dstart_all:
                self.send_one('date_start_all')
            elif cmd[0] == ControlCom.dstop_all:
                self.send_one('date_stop_all')
            elif cmd[0] == ControlCom.dstart:
                self.send('date_start', cmd[1])
            elif cmd[0] == ControlCom.dstop:
                self.send('date_stop', cmd[1])
            elif cmd[0] == ControlCom.start_all:
                self.send_one('start_all')
            elif cmd[0] == ControlCom.stop_all:
                self.send_one('stop_all')
            elif cmd[0] == ControlCom.start:
                self.send('start', cmd[1])
            elif cmd[0] == ControlCom.stop:
                self.send('stop', cmd[1])
            elif cmd[0] == ControlCom.kill_all:
                self.send_one('kill_all')
            elif cmd[0] == ControlCom.kill:
                self.send('kill', cmd[1])
            elif cmd[0] == ControlCom.chat:
                self.send('chat', cmd[1:])
            elif cmd[0] == ControlCom.chat_all:
                self.send('chat_all', cmd[1])
            elif cmd[0] == ControlCom.out:
                self.send('out', cmd[1])
            elif cmd[0] == ControlCom.input:
                self.send('input', cmd[1:], 2)
            elif cmd[0] == ControlCom.input_all:
                self.send('input_all', cmd[1])
            elif cmd[0] == ControlCom.restart_all:
                self.send_one('restart_all')
            elif cmd[0] == ControlCom.restart:
                self.send('restart', cmd[1])
            elif cmd[0] == ControlCom.lim:
                self.send('lim', cmd[1])
            elif cmd[0] == ControlCom.setlim:
                self.send('setlim', cmd[1:], 2)
            elif cmd[0] == ControlCom.spids:
                self.send_one('serv_pids')
            elif cmd[0] == ControlCom.spid:
                self.send('serv_pid', cmd[1])
            elif cmd[0] == ControlCom.ppids:
                self.send_one('proc_pids')
            elif cmd[0] == ControlCom.ppid:
                self.send('proc_pid', cmd[1])
            elif cmd[0] == ControlCom.users:
                self.send_one('users')
            elif cmd[0] == ControlCom.kick:
                self.send('kick', cmd[1])
            elif cmd[0] == ControlCom.duser:
                self.send('disable_user', cmd[1])
            elif cmd[0] == ControlCom.euser:
                self.send('enable_user', cmd[1])
            elif cmd[0] == ControlCom.npass:
                self.send('newpass', cmd[1:], 2)
            elif cmd[0] == ControlCom.npassu:
                self.send('newpass_user', cmd[1:], 2)
            elif cmd[0] == ControlCom.reg:
                self.send('reg', cmd[1:], 4)
            elif cmd[0] == ControlCom.sgroup:
                self.send('setgroup', cmd[1:], 2)
            elif cmd[0] == ControlCom.delu:
                self.send('del', cmd[1])
            elif cmd[0] == ControlCom.aperm:
                self.send('addperm', cmd[1:], 3)
            elif cmd[0] == ControlCom.dperm:
                self.send('delperm', cmd[1:], 2)
            elif cmd[0] == ControlCom.enperm:
                self.send('enable_perm', cmd[1:], 2)
            elif cmd[0] == ControlCom.diperm:
                self.send('disable_perm', cmd[1:], 2)
            elif cmd[0] == ControlCom.agroup:
                self.send('addgroup', cmd[1:], 9)
            elif cmd[0] == ControlCom.engroup:
                self.send('enable_group', cmd[1])
            elif cmd[0] == ControlCom.digroup:
                self.send('disable_group', cmd[1])
            elif cmd[0] == ControlCom.reload:
                self.send_one('reload_configs')
            elif cmd[0] == ControlCom.plugins:
                self.send_one('get_plugins')
            elif cmd[0] == ControlCom.pinfo:
                self.send('get_info', cmd[1])
            elif cmd[0] == ControlCom.load_all:
                self.send_one('load_all')
            elif cmd[0] == ControlCom.unload_all:
                self.send_one('unload_all')
            elif cmd[0] == ControlCom.load:
                self.send('load', cmd[1])
            elif cmd[0] == ControlCom.unload:
                self.send('unload', cmd[1])
            elif cmd[0] == ControlCom.preload:
                self.send('reload', cmd[1])
            elif cmd[0] == ControlCom.ver:
                self.send_one('get_version')
            elif cmd[0] == ControlCom.ping:
                self.sc.self.send('ping')
            elif cmd[0] == ControlCom.stop_panel:
                self.send_one('stop_panel')
            elif cmd[0] == ControlCom.server:
                pass
            elif cmd[0] == ControlCom.logout:
                pass
            elif cmd[0] == ControlCom.help:
                help()

        return com

    def server(self):
        def com(cmd):
            def help():
                pass

            def recv():
                while True:
                    pass

            th = threading.Thread(target=recv)
            th.start()

            if cmd[0] == ServerCom.null:
                pass
            elif cmd[0] == ServerCom.exit_:
                with main.grlock:
                    main.logger.warning(main.lang['TEXT']['exit_mess'])
                return True
            elif cmd[0] == ServerCom.help:
                help()

        return com
