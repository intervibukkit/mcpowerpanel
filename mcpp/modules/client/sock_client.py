import socket
import json
import __main__ as main
from mcpp.modules.mcpp_enums import ReqResult
from mcpp.modules.mcpp_utils import AESCip, time_limit


class SockClient:
    """Клиент для подключения к ПУ."""
    def __init__(self, encoding='utf-8', name='none', passwd='none'):
        """

        :param encoding: имя кодировки
        :param name: ник
        :param passwd: пароль
        """
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        """socket"""
        self.s.setblocking(False)
        self.enc = encoding
        """Кодировка, передаваемая в encoding."""
        self.name = name
        """Ник пользователя."""
        self.passwd = passwd
        """Пароль."""
        self.cip = AESCip(passwd=passwd, encoding=encoding)
        """AESCip."""

    def recv(self):
        """Считывание данных из сокета.

        :return: строка или None в случае ошибки
        """
        try:
            with main.grlock:
                main.logger.debug('__recv start')
                bs = int(main.config['INTERNAL']['recv_bytes'])
                rr = int(main.config['INTERNAL']['recv_repeat'])
                rw = main.config['INTERNAL']['recv_wait']
            b = self.s.recv(bs)
            if len(b) == bs:
                # получение всех данных
                ba = bytearray(b)
                i = 0
                while True:
                    try:
                        with time_limit(rw):
                            bb = self.s.recv(bs)
                            ba += bb
                            i += 1
                            if rr != 0 and i >= rr: break
                            if len(bb) != bs: break
                            else: continue
                    except TimeoutError: break
                b = ba
            if b:
                text = b.decode(self.enc)
                with main.grlock:
                    main.logger.debug(text)
                if text not in ReqResult.__members__:
                    text = self.cip.dec(text)
                return text
            else: return None
        except BaseException as be:
            with main.grlock:
                main.logger.error(str(be.args))
            return None

    def send(self, text, cip=True):
        """Отправка данных серверу.

        :param text: строка
        :param cip: bool, True - шифровать
        :return: True в случае успеха
        """
        try:
            if cip:
                self.s.sendall(self.cip.enc(text).encode(self.enc))
            else:
                self.s.sendall(text.encode(self.enc))
            return True
        except:
            with main.grlock:
                main.logger.error(main.lang['SOCK']['cbreak'])
            return False

    def auth(self):
        """Авторизация.

        :return: ответ (значение ReqResult) или None в случае ошибки
        """
        try:
            self.s.sendall(json.dumps([self.name, self.cip.enc(
                'Hello, MCPP! I\'m ' + self.name + '.')]).encode(self.enc))
            return ReqResult.__members__[self.recv()]
        except BaseException as be:
            with main.grlock:
                main.logger.error(main.lang['SOCK']['noauth'])
            return None

    def stop(self):
        """Закрытие подключения."""
        try: self.send(ReqResult.close.name)
        except: pass
        try:
            try: self.s.shutdown(socket.SHUT_RDWR)
            except: pass
            self.s.close()
        except BaseException as be:
            with main.grlock:
                main.logger.debug(json.dumps(str(be.args)))
        with main.grlock:
            main.logger.warning(main.lang['SOCK']['cbreak'])

    def start(self, host, port):
        """Подключение к серверу.

        :param host: хост сервера
        :param port: порт сервера
        :return: True в случае успеха
        """
        try:
            self.s.connect((host, port))
            return True
        except:
            with main.grlock:
                main.logger.error(main.lang['SOCK']['cno'])
            self.stop()
            return False
