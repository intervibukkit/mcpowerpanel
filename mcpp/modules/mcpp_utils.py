import signal
import time
from contextlib import contextmanager
from Crypto.Cipher import AES
from Crypto import Random
import base64
import hashlib


def get_time(tformat='%a, %d %b %Y %H:%M:%S'):
    """Получить текущее время в виде строки.

    :param tformat: формат (см. док. time.strftime),
    по-умолчанию: '%a, %d %b %Y %H:%M:%S'
    :return: текущее время в виде строки
    """
    return time.strftime(tformat, time.localtime())


@contextmanager
def time_limit(seconds): # by Josh Lee, copy from stackoverflow
    """Используется для прерывания долгих функций.
    Если лимит превышен, выбрасывается TimeoutError.

    :param seconds: лимит времени на выполнение (int)
    """
    def signal_handler(signum, frame):
        raise TimeoutError()
    signal.signal(signal.SIGALRM, signal_handler)
    signal.alarm(seconds)
    try:
        yield
    finally:
        signal.alarm(0)


class AESCip:
    # thanks to mnothic, Gregory Goltsov
    # and SquareRootOfTwentyThree from stackowverflow
    """Класс для обеспечения шифровки и дешифровки текста."""
    def __init__(self, passwd=None, hashpass=None, encoding='utf-8'):
        """Если ни пароль, ни хэш не заданы - функции enc и dec
        будут возвращать None. Используйте update_pass или update_hash.

        :param passwd: текстовый пароль
        :param hashpass: SHA-256 hex хэш пароля
        :param encoding: кодировка (по-умолчанию utf-8)
        """
        self.hexpass = None
        """Первичный hex хэш, из которого выжимается вторичный."""
        self.encoding = encoding
        """Кодировка текста."""
        if passwd: self.update_pass(passwd)
        elif hashpass: self.update_hash(hashpass)
        else: self.__hashpass = None  # используется для последующих операций

    def update_pass(self, passwd):
        """Обновление хэша.

        :param passwd: текстовый пароль
        """
        """Обновление хэша. passwd - текстовый пароль."""
        h = hashlib.sha256()
        h.update(passwd.encode(self.encoding))
        self.update_hash(h.hexdigest())

    def update_hash(self, hashpass):
        """Обновление хэша.

        :param hashpass: SHA-256 hex хэш пароля
        """
        self.hexpass = hashpass
        h = hashlib.sha256()
        h.update(hashpass.encode(self.encoding))
        self.__hashpass = h.digest()

    def _pad(self, data):
        length = AES.block_size - (len(data) % AES.block_size)
        data += bytes([length])*length
        return data

    def _unpad(self, s):
        return s[:-ord(s[len(s)-1:])]

    def enc(self, s):
        """Защифровка.

        :param s: строка
        :return: зашифрованная строка в Base85, None при исключении
        """
        if not self.__hashpass: return None
        if type(s) != str: return None
        iv = Random.new().read(AES.block_size)
        cip = AES.new(self.__hashpass, AES.MODE_CBC, iv)
        try:
            return base64.b85encode(iv + cip.encrypt(
                self._pad(s.encode(self.encoding)))).decode(self.encoding)
        except: return None

    def dec(self, s):
        """Дешифровка.

        :param s: строка
        :return: нормальная строка или None при исключении
        """
        if not self.__hashpass: return None
        if type(s) != str: return None
        enc = base64.b85decode(s)
        cip = AES.new(self.__hashpass, AES.MODE_CBC, enc[:AES.block_size])
        try:
            return self._unpad(cip.decrypt(enc[AES.block_size:]).decode(
                self.encoding))
        except: return None
