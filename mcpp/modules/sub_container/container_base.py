import logging
import json
import multiprocessing
import subprocess
import os
import signal
import __main__ as main
from mcpp.modules.mcpp_enums import EventAction, EventsPriority
from mcpp.modules.mcpp_utils import time_limit


class MultiPopen:
    """Класс с прокси переменными из multiprocessing для управления субпроцессом
    игрового сервера в Container."""
    def __init__(self):
        manager = multiprocessing.Manager()
        self.rlock = manager.RLock()
        """Блокировка, используемая при обращении к элементам класса."""
        self.pid = manager.Value(int, None)
        self.returncode = manager.Value(int, None)
        self.com = manager.Value(str, None)
        """Команда, запускаемая в субпроцессе."""
        self._kill = manager.Value(bool, False)
        self.shell = manager.Value(bool, False)
        """Запущен ли субпроцесс в шелле."""
        self.args = manager.list()
        """Аргументы, передаваемые команде."""
        self.exit_event = manager.Event()
        """Событие завершения субпроцесса (вызывается в данном классе, слушается
        в Container."""
        self.kill_event = manager.Event()
        """Событие убийства субпроцесса (вызывается в данном классе, слушается
        в Container."""

    def poll(self):
        return self.returncode.value

    def wait(self, timeout=None):
        if timeout:
            try:
                with time_limit(timeout): self.exit_event.wait()
            except TimeoutError:
                raise subprocess.TimeoutExpired(self.com.value, timeout)
        else: self.exit_event.wait()
        return self.returncode.value

    def terminate(self):
        """Метод работает с опозданием, в зависимости от cont_read_time."""
        self._kill.value = False
        self.kill_event.set()

    def kill(self):
        """Метод работает с опозданием, в зависимости от cont_read_time."""
        self._kill.value = True
        self.kill_event.set()

    def fast_kill(self, sig=signal.SIGKILL):
        """Быстрое убийство процесса (через os.kill).

        :param sig: сигнал, по-умолчанию: signal.SIGKILL
        :return: True в случае успеха
        """
        if not self.pid.value: return False
        os.kill(self.pid.value, sig)
        return True

    def is_alive(self):
        if self.exit_event.is_set(): return False
        if self.pid.value: return True
        else: return False

    def clear(self):
        """Сбросить все переменные."""
        self.exit_event.clear()
        self.kill_event.clear()
        self.pid.value = None
        self.returncode.value = None
        self.com.value = None
        self._kill.value = False
        self.shell.value = False
        for a in self.args: self.args.remove(a)


class ContainerBase:
    """Базовый класс с переменными и методами вызова плагинов."""
    def __init__(self, serv=None, name=None):
        """

        :param serv: секция из конфига servers
        :param name: используется для создания процесса
        """
        self.__manager = multiprocessing.Manager()
        self.rlock = self.__manager.RLock()
        """Блокировка из модуля multiprocessing.

        Используется при обращении к переменным и методам класса."""
        self.out = self.__manager.list()
        """Вывод консоли сервера, кол-во значений ограничено."""
        # значение для ввода в консоль
        self._in = self.__manager.Value(str, None)
        self._lim = self.__manager.Value(int, 50)  # лимит значений out
        self.dir_path = self.__manager.Value(str, None)
        """Директория сервера."""
        self.name = self.__manager.Value(str, name)
        """Имя процесса, передаваемое параметром name."""
        self.date_start = self.__manager.Value(str, 'None')
        """Дата запуска сервера."""
        self.date_stop = self.__manager.Value(str, 'None')
        """Дата остановки сервера."""
        self.SERV = serv
        """Секция из servers, переданная в параметре serv."""
        self.proc = None
        """Обслуживающий процесс с функцией _start."""
        self.subproc = MultiPopen()
        """Объект MultiPopen для управления субпроцессом игрового сервера."""
        self.wait_thread = None
        """Поток с методом waitr из wait_run."""
        self.stop_event = multiprocessing.Event()
        """Событие остановки сервера."""
        self.newout_event = multiprocessing.Event()
        """Событие добавления нового сообщения из консоли сервера в out."""
        self._newin_event = multiprocessing.Event()  # событие ввода в консоль
        self._exit_event = multiprocessing.Event()
        """Событие посыла сигнала об остановке сервера."""
        self.log = logging.getLogger(name)
        """Logger, в качестве имени используется name."""
        self.log.setLevel(logging.INFO)
        self.pls = {
            EventsPriority.high: [],
            EventsPriority.medium: [],
            EventsPriority.low: []
            }
        """Словарь объектами, унаследованные от ServPlugin.
        Ключи - EventsPriority."""

    def _put(self, mess):
        """Вызов события отправки данных в консоль.

        :param mess: строка
        :return: EventAction или произвольная строка
        """
        action = EventAction.none

        def put_(pls):
            try:
                nonlocal action
                for p in pls:
                    action = p.put(mess, action)
            except BaseException as be:
                with main.grlock:
                    main.debug.debug(json.dumps(str(be.args)))

        for p in EventsPriority.__members__:
            put_(self.pls[EventsPriority.__getitem__(p)])
        if not action: action = EventAction.none
        with main.grlock:
            if type(action) == EventAction:
                main.debug.debug(json.dumps([mess, action.name]))
            else:
                main.debug.debug(json.dumps([mess, str(action)]))
        return action

    def _chat(self, mess):
        """Вызов события отправки сообщения в чат.

        :param mess: сообщение
        :return: EventAction или произвольная строка
        """
        action = EventAction.none

        def chat_(pls):
            try:
                nonlocal action
                for p in pls:
                    action = p.chat(mess, action)
            except BaseException as be:
                with main.grlock:
                    main.debug.debug(json.dumps(str(be.args)))

        for p in EventsPriority.__members__:
            chat_(self.pls[EventsPriority.__getitem__(p)])
        if not action: action = EventAction.none
        with main.grlock:
            if type(action) == EventAction:
                main.debug.debug(json.dumps([mess, action.name]))
            else:
                main.debug.debug(json.dumps([mess, str(action)]))
        return action

    def _start_(self, serv, path):
        """Вызов события запуска метода _start.

        :param serv: секция из конфига servers
        :param path: путь к серверу
        :return: EventAction
        """
        action = EventAction.none

        def start_(pls):
            try:
                nonlocal action
                for p in pls:
                    action = p.start(serv, path, action)
            except BaseException as be:
                with main.grlock:
                    main.debug.debug(json.dumps(str(be.args)))

        for p in EventsPriority.__members__:
            start_(self.pls[EventsPriority.__getitem__(p)])
        if not action: action = EventAction.none
        with main.grlock:
            main.debug.debug(json.dumps([path, action.name]))
        return action

    def _out(self, mess):
        """Вызов события появления нового вывода в консоли.

        :param mess: вывод (строка)
        :return: EventAction или произвольная строка
        """
        action = EventAction.none

        def out_(pls):
            try:
                nonlocal action
                for p in pls:
                    action = p.out(mess, action)
            except BaseException as be:
                with main.grlock:
                    main.debug.debug(json.dumps(str(be.args)))

        for p in EventsPriority.__members__:
            out_(self.pls[EventsPriority.__getitem__(p)])
        if not action: action = EventAction.none
        with main.grlock:
            if type(action) == EventAction:
                main.debug.debug(json.dumps([mess, action.name]))
            else:
                main.debug.debug(json.dumps([mess, str(action)]))
        return action

    def _stop(self):
        """Вызов события остановки сервера (потока _start)."""
        with main.grlock: main.debug.debug('__stop')

        def stop_(pls):
            try:
                for p in pls:
                    p.stop()
            except BaseException as be:
                with main.grlock:
                    main.debug.debug(json.dumps(str(be.args)))

        for p in EventsPriority.__members__:
            stop_(self.pls[EventsPriority.__getitem__(p)])

    def _stop_server(self):
        """Вызов события остановки сервера (вызывается из _start, stop_serv)."""
        with main.grlock: main.debug.debug('__stop_server')

        def stop_serv(pls):
            try:
                for p in pls:
                    p.stop_server()
            except BaseException as be:
                with main.grlock:
                    main.debug.debug(json.dumps(str(be.args)))

        for p in EventsPriority.__members__:
            stop_serv(self.pls[EventsPriority.__getitem__(p)])

    def _start_server(self):
        """Вызов события старта сервера (_start, метод get).

        :return: EventAction
        """
        action = EventAction.none

        def start_serv(pls):
            try:
                nonlocal action
                for p in pls:
                    action = p.start_server(action)
            except BaseException as be:
                with main.grlock:
                    main.debug.debug(json.dumps(str(be.args)))

        for p in EventsPriority.__members__:
            start_serv(self.pls[EventsPriority.__getitem__(p)])
        if not action: action = EventAction.none
        with main.grlock: main.debug.debug(action.name)
        return action

    def _wait(self, sec):
        """Вызов события ожидания запуска (_start, метод wait).

        :param sec: задержка перед стартом в секундах, int
        :return: EventAction или int
        """
        action = EventAction.none

        def wait_(pls):
            try:
                nonlocal action
                for p in pls:
                    action = p.wait(sec, action)
            except BaseException as be:
                with main.grlock:
                    main.debug.debug(json.dumps(str(be.args)))

        for p in EventsPriority.__members__:
            wait_(self.pls[EventsPriority.__getitem__(p)])
        if not action: action = EventAction.none
        with main.grlock:
            if type(action) == EventAction:
                main.debug.debug(json.dumps([sec, action.name]))
            else:
                main.debug.debug(json.dumps([sec, str(action)]))
        return action

    def _run(self, serv, name):
        """Вызов события запуска метода _start в процессе.

        :param serv: секция из конфига servers
        :param name: имя процесса
        :return: EventAction
        """
        action = EventAction.none

        def run_(pls):
            try:
                nonlocal action
                for p in pls: action =\
                    p.run(serv, name, action)
            except BaseException as be:
                with main.grlock:
                    main.debug.debug(json.dumps(str(be.args)))

        for p in EventsPriority.__members__:
            run_(self.pls[EventsPriority.__getitem__(p)])
        if not action: action = EventAction.none
        with main.grlock:
            main.debug.debug(json.dumps([name, action.name]))
        return action

    def _wait_run(self, serv, name):
        """Вызов события запуска метода _start в процессе после остановки
        предыдущего.

        :param serv: секция из конфига servers
        :param name: имя процесса
        :return: EventAction
        """
        action = EventAction.none

        def wrun(pls):
            try:
                nonlocal action
                for p in pls:
                    action = p.wait_run(serv, name. action)
            except BaseException as be:
                with main.grlock:
                    main.debug.debug(json.dumps(str(be.args)))

        for p in EventsPriority.__members__:
            wrun(self.pls[EventsPriority.__getitem__(p)])
        if not action: action = EventAction.none
        with main.grlock:
            main.debug.debug(json.dumps([name, action.name]))
        return action

    def _exit_sub(self):
        """Вызов события завершения субпроцесса с сервером."""
        with main.grlock: main.debug.debug('exit_sub')

        def es(pls):
            try:
                for p in pls:
                    p.exit_sub()
            except BaseException as be:
                with main.grlock:
                    main.debug.debug(json.dumps(str(be.args)))

        for p in EventsPriority.__members__:
            es(self.pls[EventsPriority.__getitem__(p)])

    def _kill_sub(self):
        """Вызов события убийства субпроцесса сервера.

        :return: EventAction
        """
        action = EventAction.none

        def ks(pls):
            try:
                nonlocal action
                for p in pls:
                    action = p.kill_sub(action)
            except BaseException as be:
                with main.grlock:
                    main.debug.debug(json.dumps(str(be.args)))

        for p in EventsPriority.__members__:
            ks(self.pls[EventsPriority.__getitem__(p)])
        if not action: action = EventAction.none
        with main.grlock:
            main.debug.debug(json.dumps([action.name]))
        return action
