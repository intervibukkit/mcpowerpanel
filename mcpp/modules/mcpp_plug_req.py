from mcpp.modules.mcpp_enums import EventAction, EventsPriority, ReqResult


class ContainerPlugin:
    """Макет плагина для Container.

    События вызываются после совершения действия."""
    def __init__(self):
        self.NAME = 'none'
        """Название плагина."""
        self.priority = EventsPriority.low
        """EventsPriotity приоритет прослушивания событий
        (по-умолчанию: low)."""
    
    def put(self, mess, action):
        """Событие отправки данных в консоль сервера.

        :param mess: сообщение
        :param action: EventAction или произвольные данные
        :return: EventAction или произвольная строка
        """
        return EventAction.none

    def chat(self, mess, action):
        """Событие отправки сообщения в чат сервера заданной командой.

        :param mess: сообщение
        :param action: EventAction или произвольная строка
        :return: EventAction или произвольная строка
        """
        return EventAction.none

    def start(self, serv, path, action):
        """Событие старта потока обработки сервера.

        :param serv: секция из конфига servers
        :param path: путь к папке сервера
        :param action: EventAction
        :return: EventAction
        """
        return EventAction.none

    def out(self, mess, action):
        """Событие появления нового вывода в консоли сервера.

        :param mess: вывод (строка)
        :param action: EventAction или произвольная строка
        :return: EventAction или произвольная строка
        """
        return EventAction.none

    def stop(self):
        """Событие остановки потока обработки сервера (и остановка сервера)."""
        pass

    def stop_server(self):
        """Событие остановки сервера."""
        pass

    def start_server(self, action):
        """Событие запуска сервера.

        :param action: EventAction
        :return: EventAction
        """
        return EventAction.none

    def wait(self, sec, action):
        """Событие ожидания запуска сервера (пауза).

        :param sec: время ожидания в секундах (int)
        :param action: EventAction или int
        :return: EventAction или int
        """
        return EventAction.none

    def run(self, serv, name, action):
        """Событие запуса обрабатывающего процесса
        (и последующий запуск сервера).

        :param serv: секция из конфига servers
        :param name: имя процесса
        :param action: EventAction
        :return: EventAction
        """
        return EventAction.none

    def wait_run(self, serv, name, action):
        """Событие запуса обрабатывающего процесса после остановки сервера.

        :param serv: секция из конфига servers
        :param name: имя процесса
        :param action: EventAction
        :return: EventAction
        """
        return EventAction.none

    def exit_sub(self):
        """Событие завершения субпроцесса сервера."""
        pass

    def kill_sub(self, action):
        """Событие убийства субпроцесса сервера.

        :param action: EventAction
        :return: EventAction
        """
        return EventAction.none


class ServPlugin:
    """Макет плагина для ServManager.

    События вызываются после совершения действия."""
    def __init__(self):
        self.NAME = 'none'
        """Название плагина."""
        self.priority = EventsPriority.low
        """EventsPriority приоритет прослушивания событий
        (по-умолчанию: low)."""
    
    def start(self, name, result, action):
        """Событие старта сервера.

        :param name: имя сервера
        :param result: bool, True в случае успеха
        :param action: EventAction
        :return: EventAction
        """
        return EventAction.none

    def start_auto(self, action):
        """Событие стара серверов, у которых включен автозапуск.

        :param action: EventAction
        :return: EventAction
        """
        return EventAction.none

    def stop(self, name, result, action):
        """Событие остановки сервера.

        :param name: имя сервра
        :param result: bool, True в случае успеха
        :param action: EventAction
        :return: EventAction
        """
        return EventAction.none

    def kill(self, name, result, subproc, action):
        """Событие убийства процесса сервера или обслуживающего процесса.

        :param name: имя сервера
        :param result: bool, True в случае успеха
        :param subproc: True: убивается субпроцесс,
        False: обслуживающий процесс
        :param action: EventAction
        :return: EventAction
        """
        return EventAction.none


class Plugin:
    """Основной макет плагина для ПУ."""
    def __init__(self, api):
        """

        :param api: объект PluginAPI для доступа к элементам ПУ
        """
        self.NAME = 'none'
        """Название плагина."""
        self.AUTHOR = 'none'
        """Автор(ы) плагина."""
        self.EMAIL = 'none'
        """Контактный e-mail адрес."""
        self.WEBSITE = 'none'
        """Страница плагина."""
        self.VERSION = 'none'
        """Версия плагина."""
        self.LICENSE = 'GNU GPL v3'
        """Лицензия (по-умолчанию: GNU GPL v3)."""
        self.DESCRIPTION = 'none'
        """Краткое описание плагина."""
        self.priority = EventsPriority.low
        """EventsPriotity приоритет прослушивания событий
        (по-умолчанию: low)."""

    def call(self, user, data):
        """Вызов плагина клиентом.

        :param user: вызывающий пользователь
        :param data: список с данными
        :return: Возвращает нужные данные (см. док. client-server).
        """
        return None

    def func(self):
        """Функции плагина.

        Возвращает json словарь. См. документацию клиент-сервера."""
        return None

    def auth(self, user, addr, result, action):
        """Событие авторизации пользователя.

        :param user: ник пользователя
        :param addr: (IP, порт)
        :param result: первичный EventAction
        В случае result ==
        EventAction.deny, action ни на что не влияет.
        :param action: EventAction
        :return: EventAction
        """
        return EventAction.none

    def disconnect(self, user, addr):
        """Событие отключения пользователя. Ничего не возвращает.

        :param user: ник пользователя
        :param addr: (IP, порт)
        """
        pass

    def connect(self, addr, result, action):
        """Событие подключения пользователя к панели.

        :param addr: (IP, порт)
        :param result: первичный EventAction
        :param action: EventAction
        :return: EventAction
        """
        return EventAction.none

    def receive(self, user, data, action):
        """Событие получения данных от пользователя.

        :param user: ник пользователя
        :param data: данные в виде списка
        :param action: EventAction или произвольные данные
        :return: EventAction или произвольные данные
        """
        return EventAction.none

    def send(self, user, data, action):
        """Событие отправки данных пользователю.

        :param user: ник пользователя
        :param data: данные в виде списка
        :param action: EventAction или произвольные данные
        :return: EventAction или произвольные данные
        """
        return EventAction.none

    def load(self, api):
        """Событие загрузки данного плагина.

        :param api: объект PluginAPI для доступа к элементам ПУ
        :return: EventAction
        """
        return EventAction.none

    def unload(self):
        """Событие отключения плагина. Ничего не возвращает."""
        pass

    def reload(self):
        """Вызов перезагрузки конфига в плагине, если поддерживается.

        :return: ReqResult, по-умолчанию: none. При успешной
        перезагрузке необходимо вернуть good.
        """
        return ReqResult.none

    def ban(self, addr, reason, action):
        """Событие бана пользователя.

        :param addr: (IP, порт)
        :param reason: BanReason
        :param action: EventAction
        :return: EventAction
        """
        return EventAction.none

    def unban(self, addr, action):
        """Событие разбана пользователя.

        :param addr: (IP, порт)
        :param action: EventAction
        :return: EventAction
        """
        return EventAction.none

    def kick(self, addr, reason, action):
        """Событие кика пользователя.

        :param addr: (IP, порт)
        :param reason: KickReason
        :param action: EventAction
        :return: EventAction
        """
        return EventAction.none

    def ping(self, addr, user, action):
        """Событие пинга панели.

        :param addr: (IP, порт)
        :param user: ник пользователя или None (если не авторизован)
        :param action: EventAction
        :return: EventAction
        """
        return EventAction.none

    def load_plugin(self, plugin, action):
        """Событие загрузки плагина.

        :param plugin: имя плагина
        :param action: EventAction
        :return: EventAction
        """
        return EventAction.none

    def stop_panel(self, reason):
        """Событие остановки панели.

        Может вызыватся несколько раз в процессе остановки ПУ
        с разными причинами. Первая причина - истинная.

        :param reason: StopReason
        :return: StopReason
        """
        pass

    def start_panel(self, host, port, listen):
        """Событие запуска сервера панели.

        :param host: хост, с которого принимаются соединения
        :param port: порт
        :param listen: лимит пользователей онлайн
        """
        pass
