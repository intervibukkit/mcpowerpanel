from mcpp.modules.sub_server.server_base import *
import __main__ as main


class MCPPServer(MCPPServerBase):
    """Сервер на сокетах."""

    def __init__(self, mcppst, host=None, port=None, listen=None):
        """

        :param mcppst: MCPPStarter для добавления в PluginAPI
        :param host: хост, напр. 0.0.0.0
        :param port: порт
        :param listen: максимум пользователей онлайн
        """
        MCPPServerBase.__init__(self, mcppst)
        self.accth = threading.Thread(target=self.__accept)
        """Поток, обрабатывающий новые подключения."""
        self.remth = threading.Thread(target=self.__clean)
        """Поток, обрабатывающий и очищающий словари."""
        if host and port and listen:
            self.start(host, port, listen)

    def kick(self, addr, reason):
        """Отключение пользователей.

        :param addr: (IP, порт)
        :param reason: KickReason причина
        """
        with main.grlock:
            main.debug.debug(json.dumps([addr, reason]))
        action = self._kick(addr[0], reason)
        if action == EventAction.deny:
            with main.grlock:
                main.debug.debug(json.dumps(['deny', addr, reason]))
            return
        self._conn[addr[0]].sendall(
            ReqResult.timeout.name.encode(self.data.enc))
        self._conn[addr[0]].shutdown(socket.SHUT_RDWR)
        self.data.del_ip(addr[0][0])
        self.slog.warning(json.dumps([addr, ReqResult.timeout.name]))

    def __clean(self):
        """Обработка и очистка словарей."""
        try:
            while True:
                with self.rlock:
                    if self.exit_event.is_set():  # выход
                        with main.grlock:
                            main.debug.debug('_exit_event set, __clean exit')
                            self.slog.critical(main.lang['SOCK']['cth_exit'])
                        break
                    if self.data.sec_enable:
                        t = time.time()
                        # проверка списков
                        for a in self.data.auth_time.items():  # авторизующиеся
                            if int(t - a[1]) >= self.data.to_auth:
                                self.kick(a, KickReason.timeout_auth)
                        for a in self.data.conn_time.items():  # активные
                            if int(t - a[1]) >= self.data.to:
                                self.kick(a, KickReason.timeout)
                        for a in self.data.ban.items():  # забаненные
                            if int((t - a[1]) / 60) >= self.data.ban_min:
                                action = self._unban(a[0])
                                if action == EventAction.deny:
                                    continue
                                del self.data.ban_min[a[0][0]]
                                self.data.del_ip(a[0][0])
                    else:  # очистка словарей в случае отключения модуля
                        with main.grlock:
                            main.debug.debug('clean data')
                        self.data.clean_data()
                        break  # чтобы включить обратно, надо перезагрузить ПУ
                time.sleep(self.data.rtint)
        finally:
            with main.grlock:
                main.debug.debug('__clean exit')

    def __accept(self):
        """Обработка новых подключений."""
        try:
            with main.grlock:
                main.debug.debug('accept start')
            while True:
                try:
                    # принятие подключений
                    conn, addr = self._s.accept()
                    with main.grlock:
                        main.debug.debug(json.dumps(['accept', addr]))
                except OSError as ose:
                    with main.grlock:
                        main.debug.debug(
                            json.dumps(['OSError', str(ose.args)]))
                    break
                with self.rlock:
                    if self.exit_event.is_set(): break
                    # проверка на бан
                    if self.data.sec_enable and addr[0] in self.data.ban:
                        action = self._connect(addr, EventAction.deny)
                        if action != EventAction.allow:
                            conn.sendall(ReqResult.banned.name.encode(
                                self.data.enc))
                            conn.close()
                            with main.grlock:
                                main.debug.debug(json.dumps('ban', addr))
                                self.slog.warning(
                                    main.lang['SOCK']['conn_banned']
                                    + ', ' + json.dumps(addr))
                            continue
                        else:
                            with main.grlock:
                                mess = ['ban, allow', addr]
                                main.debug.debug(json.dumps(mess))
                    # проверка на лимит пользователей
                    if self.data.LISTEN != 0 and (
                                len(self._conn) >= self.data.LISTEN):
                        action = self._connect(addr, EventAction.deny)
                        if action != EventAction.allow:
                            conn.sendall(
                                ReqResult.full.name.encode(self.data.enc))
                            conn.close()
                            with main.grlock:
                                main.debug.debug(json.dumps(['listen', addr]))
                                mess = main.lang['SOCK']['conn_kick']
                                mess += ', ' + json.dumps(addr)
                                self.slog.warning(mess)
                            continue
                        else:
                            with main.grlock:
                                mess = ['listen, allow', addr]
                                main.debug.debug(json.dumps(mess))
                    # добавление данных и запуск потока обработки
                    action = self._connect(addr, EventAction.allow)
                    if action == EventAction.deny:
                        conn.close()
                        with main.grlock:
                            main.debug.debug(json.dumps(['deny', addr]))
                            self.slog.warning(main.lang['SOCK']['conn_deny']
                                              + ', ' + json.dumps(addr))
                        continue
                    with main.grlock:
                        main.debug.debug(json.dumps(['ok', addr]))
                    self._conn[addr] = conn
                    self.data.conn_time[addr] = time.time()
                    self.th.append(self._rec(conn, addr))
                with main.grlock:
                    mess = main.lang['SOCK']['conn_accept'] + ', ' + (
                        json.dumps(addr))
                    self.slog.info(mess)
                    main.debug.info(mess)
        except BaseException as be:
            with main.grlock:
                main.debug.debug(json.dumps(str(be.args)))
            with self.rlock:
                self._stop_panel(StopReason.crash)
        finally:  # выход
            with main.grlock:
                main.debug.debug('exit')
                self.slog.critical(main.lang['SOCK']['accth_end'])
            with self.rlock:
                if not self.exit_event.is_set(): self.stop()

    def stop(self):
        """Остановка сервера."""
        if not self.exit_event.is_set():
            with main.grlock:
                main.debug.debug('_stop')
            self.exit_event.set()
            for c in self._conn:
                try:
                    self._conn[c].shutdown(socket.SHUT_RDWR)
                except OSError:
                    continue
            self._s.shutdown(socket.SHUT_RDWR)
            self._s.close()
            if not self._p_stop_event.is_set():
                self._stop_panel(StopReason.other)
            self.sm.stop()
            with main.grlock:
                self.slog.critical(main.lang['SOCK']['will_stop'])
                main.debug.critical(main.lang['SOCK']['will_stop'])
        else:
            with main.grlock:
                main.debug.debug('not _exit_event is set')

    def start(self, host, port, listen):
        """Старт сервера.

        :param host: хост, с которого принимаются соединения (0.0.0.0)
        :param port: порт сервера
        :param listen: лимит пользователей онлайн
        """
        with main.grlock:
            main.debug.debug(json.dumps([host, port, listen]))
        with main.grlock:
            if main.config['LOGS']['socket'] == 'yes':
                enc = main.config['MAIN']['encoding']
                handler = logging.FileHandler(main.PATH_SOCKLOG,
                                              encoding=enc)
                handler.setLevel(logging.INFO)
                formatter = logging.Formatter(main.FORMAT)
                handler.setFormatter(formatter)
                self.slog.addHandler(handler)
        self.data.clean_data()
        self._conn.clear()
        self.th.clear()
        self._s.bind((host, int(port)))
        self._s.listen(int(listen))
        self.data.HOST = host
        self.data.PORT = int(port)
        self.data.LISTEN = int(listen)
        self._start_panel(host, port, listen)
        self.exit_event.clear()
        self.accth.start()
        self.remth.start()
        with main.grlock:
            self.slog.critical(main.lang['SOCK']['will_started'])
