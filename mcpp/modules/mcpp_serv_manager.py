import json
import __main__ as main
from mcpp.modules.mcpp_enums import EventsPriority, EventAction
from mcpp.modules.mcpp_container import Container


class ServManager:
    """Менеджер контейнеров с серверами."""

    def __init__(self):
        self.servs = {}
        """Контейнеры, где имя сервера служит ключом."""
        self.pls = {
            EventsPriority.high: [],
            EventsPriority.medium: [],
            EventsPriority.low: []
        }
        """Словарь объектами, унаследованные от ServPlugin.
        Ключи - EventsPriority."""

    def __start(self, name, result):
        """Вызов события запуска сервера.

        :param name: имя сервера
        :param result: bool, True в случае успеха
        :return: EventAction
        """
        action = EventAction.none

        def start_(pls):
            try:
                nonlocal action
                for p in pls:
                    action = p.start(name, result, action)
            except BaseException as be:
                with main.grlock:
                    main.debug.debug(json.dumps(str(be.args)))

        for p in EventsPriority.__members__:
            start_(self.pls[EventsPriority.__getitem__(p)])
        if not action: action = EventAction.none
        with main.grlock:
            main.debug.debug(json.dumps([name, result, action.name]))
        return action

    def __start_auto(self):
        """Вызов события авто-запуска серверов.

        :return: EventAction
        """
        action = EventAction.none

        def start_auto_(pls):
            try:
                nonlocal action
                for p in pls:
                    action = p.start_auto(action)
            except BaseException as be:
                with main.grlock:
                    main.debug.debug(json.dumps(str(be.args)))

        for p in EventsPriority.__members__:
            start_auto_(self.pls[EventsPriority.__getitem__(p)])
        if not action: action = EventAction.none
        with main.grlock:
            main.debug.debug(action.name)
        return action

    def __stop(self, name, result):
        """Вызов события остановки сервера.

        :param name: имя сервера
        :param result: bool, True в случае успеха
        :return: EventAction
        """
        action = EventAction.none

        def stop_(pls):
            try:
                nonlocal action
                for p in pls:
                    action = p.stop(name, result, action)
            except BaseException as be:
                with main.grlock:
                    main.debug.debug(json.dumps(str(be.args)))

        for p in EventsPriority.__members__:
            stop_(self.pls[EventsPriority.__getitem__(p)])
        if not action: action = EventAction.none
        with main.grlock:
            main.debug.debug(json.dumps([name, result, action.name]))
        return action

    def __kill(self, name, result, subproc):
        """Вызов события убийства субпроцесса или процесса.

        :param name: имя сервера
        :param result: bool, True в случае успеха
        :param subproc: bool, True - если убивается субпроцесс, False - процесс
        :return: EventAction
        """
        action = EventAction.none

        def kill_(pls):
            try:
                nonlocal action
                for p in self.pls:
                    action = p.kill(name, result, subproc, action)
            except BaseException as be:
                with main.grlock:
                    main.debug.debug(json.dumps(str(be.args)))

        for p in EventsPriority.__members__:
            kill_(self.pls[EventsPriority.__getitem__(p)])
        if not action: action = EventAction.none
        with main.grlock:
            main.debug.debug(
                json.dumps([name, result, subproc, action.name]))
        return action

    def start(self, name=None):
        """Запуск сервера.

        :param name: имя сервера, если None - все сервера
        :return: True в случае успеха
        """
        def start_(name):
            """Проверка и запуск остановленного сервера.

            :param name: имя сервера
            :return: True в случае успеха
            """
            st = False
            if name in self.servs:
                serv = self.servs[name]
                try:
                    serv.rlock.acquire()
                    if serv.proc and not serv.proc.is_alive():
                        if self.__start(name, True) == EventAction.deny:
                            return st
                        serv.run(main.servers[name], name)
                        st = True
                finally:
                    serv.rlock.release()
            else:
                with main.grlock:
                    s = main.servers[name]
                if name not in s:
                    self.__start(name, False)
                    return st
                if self.__start(name, True) == EventAction.deny: return st
                self.servs[name] = Container(main.servers[name], name)
                st = True
            main.debug.info(json.dumps([name, st]))
            return st

        if name:
            with main.grlock:
                return start_(name)
        else:
            res = False
            with main.grlock:
                for s in main.servers:
                    if s == 'DEFAULT': continue
                    res = start_(s)
            return res

    def start_auto(self):
        """Старт всех серверов, у которых включен auto_load."""
        if self.__start_auto() == EventAction.deny: return
        with main.grlock:
            main.debug.info('start_auto')
            for s in main.servers:
                if s == 'DEFAULT': continue
                if main.servers[s]['auto_load'] == 'yes':
                    if self.__start(s, True) == EventAction.deny: return
                    self.servs[s] = Container(main.servers[s], s)
                else:
                    if self.__start(s, False) == EventAction.allow:
                        self.servs[s] = Container(main.servers[s], s)

    def stop(self, name=None):
        """Остановка запущенного сервера.

        :param name: имя сервера, если None - все сервера
        :return: True в случае успеха
        """

        def stop_(name):
            """Остановить запущенный сервер.

            :param name: имя сервера
            :return: True в случае успеха
            """
            st = False
            if name in self.servs:
                try:
                    self.servs[name].rlock.acquire()
                    if not self.servs[name].stop_event.is_set():
                        if self.__stop(name, True) == EventAction.deny:
                            return st
                        self.servs[name].stop()
                        st = True
                finally:
                    self.servs[name].rlock.release()
            if not st:
                if self.__stop(name, False) == EventAction.deny: return st
            with main.grlock:
                main.debug.info(json.dumps([name, st]))
            return st

        if name:
            return stop_(name)
        else:
            res = False
            for ser in self.servs: res = stop_(ser)
            return res

    def kill(self, name=None):
        """Убийство процессов сервера.

        :param name: имя сервера, если None - все сервера
        :return: True в случае успеха
        """

        def kill_(name):
            """Убить процессы.

            :param name: имя сервера
            :return: True в случае успеха
            """
            ki = False
            if name not in self.servs: return ki
            serv = self.servs[name]
            try:
                serv.rlock.acquire()
                if serv.subproc.is_alive():
                    if self.__kill(name, True, True) == EventAction.deny:
                        return ki
                    serv.subproc.fast_kill()
                    ki = True
                else:
                    self.__kill(name, False, True)
                if serv.proc and serv.proc.is_alive():
                    if self.__kill(name, True, False) == EventAction.deny:
                        return ki
                    serv.proc.terminate()
                    ki = True
                else:
                    self.__kill(name, False, False)
            finally:
                serv.rlock.release()
            with main.grlock:
                main.debug.info(json.dumps([name, ki]))
            return ki

        if name:
            return kill_(name)
        else:
            res = False
            for ser in self.servs: res = kill_(ser)
            return res
