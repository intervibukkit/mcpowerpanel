# for Python 3.4.2
# License: GNU GPL v3
# Author: InterVi (progir@ya.ru, vomine@ya.ru)
# Version: 1.0a2
# в процессе...

import logging
import sys
import os
import configparser
import threading
from mcpp.modules.client.debug_client import DebugMode
# from mcpp.modules.client.text_client import TextClient

config = configparser.ConfigParser()
"""Главный конфиг клиента."""
lang = configparser.ConfigParser()
"""Конфиг с локализацией."""

# пути к директориям
THIS_PATH = os.path.dirname(os.path.abspath(sys.argv[0]))
PATH_DIR = os.path.join(THIS_PATH, 'mcpp')
PATH_CONFS = os.path.join(PATH_DIR, 'configs')
PATH_LOGS = os.path.join(PATH_DIR, 'logs')
PATH_LANGS = os.path.join(PATH_DIR, 'langs')
# пути к конфигам
PATH_CONF = os.path.join(PATH_CONFS, 'mcpp_client.conf')
PATH_LANG = os.path.join(PATH_LANGS, 'client_ru.conf')
# пути к логам
PATH_LOG = PATH_LOGS + os.sep + 'mcpp_client.log'
FORMAT = '{%(levelname)s [%(asctime)s]}: %(message)s'
# форматтеры
FORMAT_FILE = ('{[%(levelname)s] [%(filename)s] [%(funcName)s] ' +
               '[%(processName)s|%(threadName)s] [%(lineno)d] ' +
               '[%(asctime)s]}: %(message)s')

log_stream = open(os.devnull, 'a')
logging.basicConfig(format=FORMAT, stream=log_stream)
logger = logging.getLogger('MCPPClient')
logger.setLevel(logging.DEBUG)
console_handler = logging.StreamHandler(sys.stdout)
console_handler.setLevel(logging.INFO)
console_handler.setFormatter(logging.Formatter(FORMAT))
file_handler = logging.FileHandler(filename=PATH_LOG)
file_handler.setLevel(logging.DEBUG)
file_handler.setFormatter(logging.Formatter(FORMAT_FILE))
logger.addHandler(console_handler)
logger.addHandler(file_handler)

grlock = threading.RLock()


def _config():
    """Создание и загрузка конфигов.

    :return: True, если файлы были созданы
    """
    exit_ = False
    # создание папок
    if not os.path.isdir(PATH_CONFS):
        os.makedirs(PATH_CONFS)
        exit_ = True
    if not os.path.isdir(PATH_LOGS):
        os.makedirs(PATH_LOGS)
        exit_ = True
    if not os.path.isdir(PATH_LANGS):
        os.makedirs(PATH_LANGS)
        exit_ = True
    # конфиг локализации
    global PATH_LANG
    PATH_LANG = os.path.join(PATH_LANGS, config['MAIN']['lang'] + '.conf')
    if not os.path.isfile(PATH_LANG):
        logger.critical(PATH_LANG + ' not found')
        exit_ = True
    else:
        lang.read(PATH_LANG)
    # главный конфиг
    if not os.path.isfile(PATH_CONF):
        config['MAIN'] = {
            'encoding': 'utf-8',
            'savepass': 'yes',
            'host': 'localhost',
            'port': '25500',
            'lang': 'client_ru'
            }
        config['USERS'] = {
            'ROOT': 'pass'
            }
        config['INTERNAL'] = {
            'recv_bytes': '4096',
            'recv_repeat': '0',
            'recv_wait': '0.5'
            }
        with open(PATH_CONF, 'w') as config_file: config.write(config_file)
        logger.critical(lang['MAIN']['conf'])
        exit_ = True
    else:
        config.read(PATH_CONF)
    return exit_


if __name__ == '__main__':
    if _config(): sys.exit()
    dm = DebugMode()
    dm.start()
