# for Python 3.4.2
# License: GNU GPL v3
# Author: InterVi (progir@ya.ru, vomine@ya.ru)
# Version: 1.0a4

import threading
import configparser
import logging
import sys
import os
import json
from getpass import getpass
from mcpp.modules.mcpp_server import MCPPServer
from mcpp.modules.mcpp_utils import AESCip

# пути к директориям
THIS_PATH = sys.path[0]
PATH_DIR = os.path.join(THIS_PATH, 'mcpp')
PATH_CONFS = os.path.join(PATH_DIR, 'configs')
PATH_LOGS = os.path.join(PATH_DIR, 'logs')
PATH_PLUGINS = os.path.join(THIS_PATH, 'mcpp_plugins')
PATH_LANGS = os.path.join(PATH_DIR, 'langs')
# пути к конфигам
PATH_CONF = os.path.join(PATH_CONFS, 'mcpp_conf.conf')
PATH_SERVERS = os.path.join(PATH_CONFS, 'mcpp_servers.conf')
PATH_GROUPS = os.path.join(PATH_CONFS, 'mcpp_groups.conf')
PATH_USERS = os.path.join(PATH_CONFS, 'mcpp_users.conf')
PATH_LANG = os.path.join(PATH_LANGS, 'ru.conf')
# пути к логам
PATH_SOCKLOG = os.path.join(PATH_LOGS, 'mcpp_socket.log')
PATH_WORKLOG = os.path.join(PATH_LOGS, 'mcpp_worker.log')
PATH_DEBUG = os.path.join(PATH_LOGS, 'mcpp_debug.log')
PATH_PLUGLOG = os.path.join(PATH_LOGS, 'mcpp_plugins.log')
PATH_STDERR = os.path.join(THIS_PATH, 'mcpp_stderr.log')
"""Пути к логам."""
FORMAT = '{%(levelname)s [%(asctime)s]}: %(message)s'
"""Формат для Formatter всех логгеров, кроме debug."""
FORMAT_DEBUG = ('{[%(levelname)s] [%(filename)s] [%(funcName)s] ' +
                '[%(processName)s|%(threadName)s] [%(lineno)d] ' +
                '[%(asctime)s]}: %(message)s')
CONSOLE_FORMAT = '[%(levelname)s] [%(funcName)s]: %(message)s'
"""Формат для Formatter для debug логгера."""
VERSION = '1.0a4'
"""Версия ПУ."""

config = configparser.ConfigParser()
"""Главный конфиг ПУ."""
servers = configparser.ConfigParser()
"""Конфиг с серверами"""
groups = configparser.ConfigParser()
"""Конфиг с группами пользователей."""
users = configparser.ConfigParser()
"""Конфиг с пользователями."""
lang = configparser.ConfigParser()
"""Конфиг с локализацией."""
grlock = threading.RLock()
"""Используется при обращении к переменным ПУ, таким, как конфиги."""

# логгеры
log_stream = open(os.devnull, 'a')
"""Поток для вывода логов, по-умолчанию: open(os.devnull, 'a')."""
logging.basicConfig(format=FORMAT, stream=log_stream)
debug = logging.getLogger('MCPowePanel')
"""Логгер для debug логгинга."""
debug.setLevel(logging.DEBUG)
_debug_handler = None
_console_handler = logging.StreamHandler(sys.stdout)
"""Handler-ы для debug логгинга."""
_console_handler.setLevel(logging.INFO)
_console_handler.setFormatter(logging.Formatter(CONSOLE_FORMAT))
debug.addHandler(_console_handler)


def _debug(enable):
    """Включение и выключение debug логгинга."""
    global _debug_handler
    if enable:
        if not _debug_handler:
            enc = config['MAIN']['encoding']
            _debug_handler = logging.FileHandler(filename=PATH_DEBUG,
                                                 encoding=enc)
            _debug_handler.setLevel(logging.DEBUG)
            _debug_handler.setFormatter(logging.Formatter(FORMAT_DEBUG))
            debug.addHandler(_debug_handler)
    else:
        if _debug_handler:
            debug.removeHandler(_debug_handler)
            _debug_handler = None


def _config():
    """Создание и загрузка конфигов.

    :return: True, если файлы были созданы"""
    exit_ = False
    # создание папок
    if not os.path.isdir(PATH_CONFS):
        os.makedirs(PATH_CONFS)
        exit_ = True
    if not os.path.isdir(PATH_LOGS):
        os.makedirs(PATH_LOGS)
        exit_ = True
    if not os.path.isdir(PATH_LANGS):
        os.makedirs(PATH_LANGS)
        exit_ = True
    # конфиг локализации
    global PATH_LANG
    PATH_LANG = os.path.join(PATH_LANGS, config['MAIN']['lang'] + '.conf')
    if not os.path.isfile(PATH_LANG):
        debug.critical(PATH_LANG + ' not found')
        exit_ = True
    else:
        lang.read(PATH_LANG)
    # главный конфиг
    if not os.path.isfile(PATH_CONF):
        config['MAIN'] = {  # главная секция главного конфига
            'encoding': 'utf-8',  # кодировка файлов, вывода и т.д.
            'plugins': 'yes',  # загружать ли плагины
            'host': '0.0.0.0',  # хост сервера на сокетах
            'port': '25500',  # порт сервера
            'listen': '10',  # макс. число клиентов
            'lang': 'ru'  # имя файла локализации
            }
        config['ROOT'] = {  # главный пользователь панели с полным доступом
            'host': 'localhost',  # хост, с которого можно входить под ROOT
            'enable': 'yes',  # включен ли ROOT
            'max_try': '3',  # макс. кол-во неудачных попыток дешифровки
            'pass': 'hashpass'  # хеш пароля от ROOT
            }
        config['SECURITY'] = {  # настройки безопасности
            'enable': 'yes',  # включен ли данный функционал
            'max_try': '5',  # макс. кол-во неудачных попыток дешифровки
            'max_login_err': '5',  # макс. ошибок при авторизации
            'max_err': '10',  # макс. кол-во ошибок при "общении"
            'ban_mins': '5',  # время бана в минутах
            'timeout': '180',  # таймаут молчания при "общении"
            'timeout_auth': '5',  # таймаут молчания при авторизации
            'show_ver': 'yes'  # показывать ли версию клиентам
            }
        config['LOGS'] = {  # логи
            'socket': 'yes',
            'worker': 'yes',
            'debug': 'no',
            'plugins': 'no'
            }
        config['INTERNAL'] = {  # внутренние настройки
            'cont_read_time': '1',
            # время считывания данных с консоли, после чего цикл прокручивается
            'sock_recv_wait': '0.5',  # таймайт получения партии полных данных
            'sock_remth_int': '1',  # интервал проверки словарей
            'recv_bytes': '4096',  # сколько читать байт из сокета
            'recv_repeat': '0'  # макс. кол-во дополнительных партий данных
            }
        with open(PATH_CONF, 'w') as config_file:
            config.write(config_file)
        debug.warning(lang['MAIN']['conf'])
        exit_ = True
    else:
        config.read(PATH_CONF)
    # конфиг серверов
    if not os.path.isfile(PATH_SERVERS):
        servers['SERVER'] = {
            'wait_sec_restart': '10',  # сколько ждать перед рестартом
            'stop_sec_timeout': '30',
            # таймайт остановки, после которого убийство процесса
            'path': './servers/server',
            # директория сервера, . означает папку с MCPP
            'shell': 'no',  # запускать ли команду в /bin/sh
            'run_command': 'java -jar spigot.jar',  # команда запуска
            'stop_command': 'stop',  # команда остановки
            'chat_command': 'say $mess',  # команда отправки сообщения в чат
            'restart': 'yes',  # перезапускать ли сервер в случае остановки
            'log': 'yes',  # вести ли лог
            'auto_load': 'yes',  # запускать ли со стартом панели
            'log_file': 'mcpp_server.log',  # имя лога сервера
            'log_errors': 'yes',  # вести ли лог ошибок Python
            'log_errors_file': 'mcpp_errors.log'  # имя лога ошибок
            }
        with open(PATH_SERVERS, 'w') as servers_:
            servers.write(servers_)
        debug.warning(lang['MAIN']['server'])
        exit_ = True
    else:
        servers.read(PATH_SERVERS)
    # конфиг групп пользователей
    if not os.path.isfile(PATH_GROUPS):
        groups['OWNERS'] = {  # название группы
            'enable': 'yes',  # включена ли группа
            'servers': '.',  # . значает доступ ко всем серверам
            'plugins': '.',  # . означает доступ по всем плагинам
            'com_list': '.',  # . разрешает использование всех команд
            'cons_see': 'yes',  # разрешено ли читать консоль
            'chat_speak': 'yes',  # разрешено ли писать в чат
            'control': 'yes',  # разрешено ли управлять сервером
            'stats': 'yes'  # разрешено ли видеть различные данные
            }
        groups['ADMINS'] = {
            'enable': 'yes',
            'servers': '.',
            'plugins': 'first_plugin|two_plugin',
            'com_list': '.',
            'cons_see': 'yes',
            'chat_speak': 'yes',
            'control': 'yes',
            'stats': 'yes'
            }
        groups['MODERS'] = {
            'enable': 'yes',
            'servers': '.',
            'plugins': 'first_plugin|two_plugin',
            'com_list': 'ban|pardon|mute|kick',
            'cons_see': 'yes',
            'chat_speak': 'yes',
            'control': 'no',
            'stats': 'yes'
            }
        groups['HELPERS'] = {
            'enable': 'yes',
            'servers': 'SERVER|SERVER2',
            'plugins': 'first_plugin|two_plugin',
            'com_list': 'mute|unmute',
            'cons_see': 'yes',
            'chat_speak': 'yes',
            'control': 'no',
            'stats': 'yes'
            }
        groups['SPY'] = {
            'enable': 'yes',
            'servers': 'SERVER',
            'plugins': '-',
            'com_list': '-',
            'cons_see': 'no',
            'chat_speak': 'no',
            'control': 'no',
            'stats': 'yes'
            }
        with open(PATH_GROUPS, 'w') as groups_:
            groups.write(groups_)
        debug.warning(lang['MAIN']['groups'])
        exit_ = True
    else:
        groups.read(PATH_GROUPS)
    # конфиг пользователей
    if not os.path.isfile(PATH_USERS):
        users['InterVi'] = {  # ник
            'group': 'OWNERS',  # группа
            'enable': 'yes',  # включен ли пользователь
            'pass': 'mypass or hash my pass'  # пароль или хеш пароля
            }
        with open(PATH_USERS, 'w') as users_:
            users.write(users_)
        debug.warning(lang['MAIN']['users'])
        exit_ = True
    else:
        users.read(PATH_USERS)
    # включение/выключение debug логгинга
    if config['LOGS']['debug'] == 'yes':
        _debug(True)
    else:
        _debug(False)
    debug.debug(str(exit_))
    return exit_


class MCPPStarter:
    """Главный класс, запускающий и останавливающий панель."""
    def __init__(self):
        self.server = MCPPServer(self)
        """MCPPServer."""
        self.server.api.mcppstarter = self
        self.stop_wait_th = threading.Thread(target=self.__stop_wait)
        """Поток, останавливающий панель при срабатывании события

        _p_stop_event из server."""
        self.__start = False  # запущена ли панель
        self.rlock = threading.RLock()
        """Блокировка, используется для доступа к элементам класса."""

    def __stop_wait(self):
        """Ожидание события остановки ПУ."""
        try:
            with grlock:
                debug.debug('stop waiting...')
            self.server._p_stop_event.wait()
            with grlock:
                debug.debug('stop_wait go')
            with self.rlock:
                if not self.__start: return
                if self.__start: self.stop()
        except BaseException as be:
            with grlock:
                debug.debug(json.dumps(['except', str(be.args)]))

    @staticmethod
    def __check():
        """Проверка прав на чтение и запись.

        :return: True, если всё в порядке
        """
        def __ch_file(path):
            """Проверка прав на чтение и запись файла.

            :param path: путь к файлу
            :return: True, если всё в порядке
            """
            if os.path.isfile(path) and not os.access(
                    path, os.R_OK) and not (os.access(path, os.W_OK)):
                return False
            else:
                return True

        def __ch_dir(path):
            """Проверка прав на чтение директории.

            :param path: путь к директории
            :return: True, если всё в порядке
            """
            if os.path.isdir(path) and not os.access(path, os.R_OK):
                return False
            return True
        
        if not os.access(THIS_PATH, os.R_OK) or not os.access(
                THIS_PATH, os.W_OK):
            return False
        path_files = (PATH_CONF, PATH_SERVERS, PATH_GROUPS, PATH_USERS,
                      PATH_SOCKLOG, PATH_WORKLOG, PATH_DEBUG, PATH_PLUGLOG)
        path_dirs = (PATH_PLUGINS, PATH_DIR, PATH_CONFS, PATH_LOGS, PATH_LANGS)
        for p in path_files:
            if not __ch_file(p):
                return False
        for p in path_dirs:
            if not __ch_dir(p):
                return False
        return True

    def _stop(self):
        """Вызывается при KeyboardInterrupt."""
        if not self.__start:
            debug.critical(lang['MAIN']['only_stop'])
            return
        self.server.unload()
        self.__start = False
        self.server._p_stop_event.set()

    def stop(self):
        """Остановка панели."""
        if not self.__start:
            debug.critical(lang['MAIN']['only_stop'])
            return
        try:
            self.server.stop()
            self.server.unload()
        except BaseException as be:
            debug.debug(json.dumps(str(be.args)))
        self.__start = False
        self.server._p_stop_event.set()
        self.server.accth.join()
        self.server.remth.join()
        for t in self.server.th: t.join()
        for c in self.server.sm.servs.values():
            if c.proc:
                c.proc.join()
        debug.critical(lang['MAIN']['p_stop'])

    def start(self):
        """Старт панели. Загрузка плагинов, сервера и серверов игры."""
        if self.__start:
            debug.critical(lang['MAIN']['only_start'])
            return False
        if not self.__check():
            debug.critical(lang['MAIN']['noperm'])
            return False
        if not os.path.isdir(PATH_PLUGINS): os.makedirs(PATH_PLUGINS)
        self.__start = True
        try:
            self.server.load()
        except BaseException as be:
            debug.debug(json.dumps(str(be.args)))
        try:
            self.server.start(config['MAIN']['host'], config['MAIN']['port'],
                              config['MAIN']['listen'])
        except OSError as ose:
            self.server.unload()
            self.server.stop()
            debug.critical(lang['MAIN']['oserror'])
            debug.error(json.dumps(str(ose.args)))
            return False
        self.server.sm.start_auto()
        self.server._p_stop_event.clear()
        self.stop_wait_th.start()
        debug.critical(lang['MAIN']['p_start'])
        return True


def __start():
    """Запуск панели."""
    starter = None
    try:
        sys.stderr = open(PATH_STDERR, 'a')
        if _config():
            return
        if config['ROOT']['pass'] == 'hashpass':
            i = 0
            while True:
                p1 = getpass(lang['MAIN']['ipass'] + ': ')
                p2 = getpass(lang['MAIN']['ipass2'] + ': ')
                if p1 != p2:
                    debug.critical(lang['MAIN']['diffpass'])
                else:
                    config['ROOT']['pass'] = AESCip(passwd=p1).hexpass
                    with open(PATH_CONF, 'w') as config_file:
                        config.write(config_file)
                    break
                i += 1
                if i >= 3:
                    sys.exit()
        starter = MCPPStarter()
        starter.start()
        starter.stop_wait_th.join()
    except KeyboardInterrupt:  # корректная остановка при CTRL+C
        debug.critical(lang['MAIN']['ctrl_c'])
        if starter:
            starter._stop()
    except BaseException as be:
        debug.critical(str(be.args))
        if starter:
            starter._stop()

if __name__ == '__main__':  # запуск панели
    __start()
